# mangrove 🌳

Experimental bytecode interpreter with the goal to be tiny yet fully functional.

//TODO: add nice picture :)

The project's purpose is to learn about the JVM internals and iteratively build a simple interpreter for bytecode.
It's not intended as serious replacement for existing implementations of the JVM.

## approach

The idea is to implement the
[Java SE 17 JVM specificiation](https://docs.oracle.com/javase/specs/jvms/se17/html/index.html)
step by step, in order to gain knowledge about the way a bytecode interpreter works. The implementation
language is going to be Rust.

The rough steps will be:
- [x] parse .class files
- [x] create classes from class file information
- [x] create environment to manage classes
- [x] build minimalistic interpreter (i.e. implement only strictly needed opcodes)
- [x] celebrate first interpreted code 🎉
- [x] implement loading of multiple classes
- [x] load java language base classes
- [x] the concept of instances
- [x] implement native methods
- [x] implement proper loading of classes (i.e. run init methods)
- [ ] extend range of interpreted opcodes
- [ ] extend range of loaded JRE classes
  - [ ] investigate possibility to re-implement JRE classes, in order to not implement so many openJDK specialities
- [ ] implement monitors / locks / notify / wait
- [ ] implement threads
- [ ] implement exceptions
- [ ] garbage collection

## non-functional requirements

The implementation mainly aims to be well modularised and without unsafe code. Efficiency and performance are a concern as well, and the intention is not actively diminish them. The project also tries to use as few dependencies as possible.

Additionally, the implementation will be rather "strict", in a sense that it shows fail-fast behaviour, and will abort on unexpected or unimplemented events.

## finer todo
- [x] parse .class files
- [x] derive class representation from class files
- [x] implement runtime that manages loaded classes
- [x] enhance runtime with interpreter, that runs code from methods of class files
- [x] interpret first simple method (static, adding two ints and return the result)
- [x] loading classes
- [x] how to get "standard" classes in, like Object etc.?
- [x] the concept of instances
- [x] check invokespecial and invokevirtual parameter lookup (correct order of arguments and reference)
- [ ] loading java/lang/String ? :)
  - [ ] putting constant_pool value as java.lang.String
  - [x] hunt the create new instance / dup / invokespecial bug -> it was the reversed order of parameters from the stack of invokespecial
  - [x] loading java/lang/Class
  - [x] ldc opcode (pushing the correct reference to Class<clazz>)
  - [x] refactor array handling (create instances, instead of storing values directly)
  - [x] support String parameters of main(String[] args)
- [ ] interpreting the standard "hello world" program, i.e. being able to write to the standard output via System.out.println("")
  - [ ] load needed classes
  - [x] fix field loading for instances (need to include fields from superclasses and interfaces)
  - [x] fix possibility to provide subtypes of parameters (e.g. provide ByteArrayOutputStream for OutputStream parameter)
  - [ ] initialise the PrintOutputStream of System.out
  - [ ] connect to interpreter's standard out via native implementation
- [x] invokestatic opcode
- [ ] clean up logs
- [ ] initialising classes
  - [x] recursive init
  - [ ] dynamic initialising?
- [x] field lookup
- [ ] refine method lookups for different invocation opcodes
- [x] implement opcodes to run <init> methods
- [x] implement native methods
- [x] load JARs (or folders!)
- [x] access flags
- [ ] extend range of interpreted opcodes
  - [ ] currently 85/149 implemented (partially with many TODOs)
- [ ] extend range of loaded JRE classes
  - [ ] currently 30 classes from java.base module loaded
  - [ ] load all classes from java.base
  - [ ] load all JRE classes from all modules
- [ ] clean up class file modules and impls
- [ ] validate fields parsing with proper class file
- [ ] more unit tests

## How do I...

### extract classes from jre modules?

For this project, [Eclipse Temurin](https://adoptium.net/) has been used. In order to see and extract all .class files of the java.base module for a given installation, do the following:

```bash
jimage list --include='/java.base**' $JAVA_HOME/lib/modules
```
then
```bash
jimage extract --include='/java.base**' --dir=./assets/jre-modules/ $JAVA_HOME/lib/modules
```

If there is no JDK available on your system, you can use the provided script to download the Eclipse Temurin JDK, extract the java.base module, and remove the downloaded JDK again:
```bash
./get-jre-modules.sh
```
