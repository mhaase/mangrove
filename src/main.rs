use std::env;
use std::io::Result;

fn main() -> Result<()> {
    let args: Vec<String> = env::args().skip(1).collect();
    let provided_classes = match args.get(0) {
        Some(arg) => arg,
        None => {
            let default = "test-assets/CurrentStarter.class";
            println!("no classes to load provided. Use comma-separated list of class files as first parameter. Using default: {}", default);
            default
        }
    };
    let class_name = match args.get(1) {
        Some(arg) => arg,
        None => {
            let default = "mangrove/test/CurrentStarter";
            println!("no class for method invocation provided. Use class name as second parameter. Using default: {}", default);
            default
        }
    };

    let parameters = if args.len() > 2 { &args[2..] } else { &[] };

    match mangrove::startup_runtime(provided_classes, class_name, parameters) {
        Ok(_runtime) => Ok(()),
        Err(e) => Err(e),
    }
}
