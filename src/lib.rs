use std::{fs, io::Result, path::Path};

use runtime::Runtime;

mod class_loader;
pub mod runtime;

pub fn startup_runtime(
    provided_classes: &str,
    class_name: &str,
    parameters: &[String],
) -> Result<Runtime> {
    let mut runtime = Runtime::new();
    // bootstrap runtime
    let bootstrap_classes_dir = "./assets/jre-modules/java.base/";
    let bootstrap_classes = vec![
        "java/lang/Object.class",
        "java/lang/String.class",
        "java/lang/String$CaseInsensitiveComparator.class",
        "java/util/Comparator.class",
        "java/io/Serializable.class",
        "java/lang/StringUTF16.class",
        "java/lang/Comparable.class",
        "java/lang/CharSequence.class",
        "java/lang/constant/Constable.class",
        "java/lang/constant/ConstantDesc.class",
        "java/lang/Class.class",
        "java/lang/ClassLoader.class",
        "java/lang/System.class",
        "java/io/PrintStream.class",
        "java/lang/Appendable.class",
        "java/io/Closeable.class",
        "java/lang/AutoCloseable.class",
        "java/io/FileDescriptor.class",
        "java/io/BufferedOutputStream.class",
        "java/io/FilterOutputStream.class",
        "java/io/OutputStream.class",
        "java/io/File.class",
        "java/io/OutputStreamWriter.class",
        "java/io/Writer.class",
        "java/nio/charset/Charset.class",
        "sun/security/action/GetPropertyAction.class",
        "java/util/Properties.class",
        "java/util/Hashtable.class",
        "java/util/Dictionary.class",
        "java/util/Map.class",
        // "java/lang/Integer.class",
        // "java/lang/Long.class",
        // "java/lang/Double.class",
        // "java/lang/Float.class",
        // "java/lang/Number.class",
        // "java/lang/Math.class",
        // "java/lang/Math$RandomNumberGeneratorHolder.class",
        // "java/io/OutputStream.class",
        // "java/io/OutputStream$1.class",
    ];
    println!("loading bootstrap classes:");
    load_bootstrap_classes(&mut runtime, bootstrap_classes_dir, bootstrap_classes);

    // load provided classes
    println!("loading provided classes:");
    for class_path in provided_classes.split(',') {
        runtime.load(Path::new(class_path))?;
    }

    println!("initialising loaded classes:");
    runtime.init_all_classes();

    println!("\n\ninvoke main of {}", &class_name);
    runtime.invoke_main(class_name, parameters);

    Ok(runtime)
}

fn find_class_files(dir: &Path) -> Result<Vec<String>> {
    let mut files = Vec::new();
    let dir_path = Path::new(dir);

    for entry in fs::read_dir(&dir_path)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_dir() {
            files.append(&mut find_class_files(&path)?);
            continue;
        }
        match path.extension() {
            Some(extension) => {
                if extension.eq("class") {
                    files.push(path.to_str().unwrap().to_owned());
                }
            }
            None => continue,
        }
    }

    Ok(files)
}

/// will recursively scan the bootstrap_classes_dir for class files, if the provided files vector is empty
///
fn load_bootstrap_classes(runtime: &mut Runtime, bootstrap_classes_dir: &str, files: Vec<&str>) {
    let mut classes: Vec<String> = files.iter().map(|s| s.to_string()).collect();
    if classes.is_empty() {
        let mut class_files = match find_class_files(&Path::new(bootstrap_classes_dir)) {
            Ok(value) => value,
            Err(e) => panic!("could not load bootstrap classes: {}", e),
        };
        classes.append(&mut class_files);
    } else {
        classes = classes
            .iter()
            .map(|str| format!("{}{}", bootstrap_classes_dir, str))
            .collect();
    }
    classes
        .iter()
        .map(|str| {
            Path::new(&str)
                .canonicalize()
                .expect(&format!("could not locate bootstrap class: {}", str))
        })
        .for_each(|path| {
            runtime.load(&path).expect(&format!(
                "could not load bootstrap class into runtime: {}",
                &path.display()
            ));
        });
}

#[cfg(test)]
mod test {
    use crate::class_loader::class::TypedValue;

    #[test]
    fn run_start_addition() {
        // easier than manual invocation on the command line :)
        // command at time of writing the test would be:
        // $ cargo run test-assets/00-start/Addition.class mangrove/test/start/Addition
        let provided_classes = vec!["test-assets/00-start/Addition.class"].join(",");

        let class_name = "mangrove/test/start/Addition";

        let mut runtime = crate::startup_runtime(&provided_classes, class_name, &vec![]).unwrap();

        let result = runtime.invoke_static(
            class_name,
            "add",
            vec![TypedValue::Int(3), TypedValue::Int(8)],
        );
        match result {
            TypedValue::Int(x) => assert_eq!(11, x),
            _ => panic!("expected Int result"),
        }
    }

    #[test]
    fn run_inheritance_tests1() {
        // command at time of writing the test would be:
        // $ cargo run test-assets/01-inheritance/A.class,test-assets/01-inheritance/B.class,test-assets/01-inheritance/C.class mangrove/test/inheritance/C
        let provided_classes = vec![
            "test-assets/01-inheritance/A.class",
            "test-assets/01-inheritance/B.class",
            "test-assets/01-inheritance/C.class",
        ]
        .join(",");

        let class_name = "mangrove/test/inheritance/C";

        let mut runtime = crate::startup_runtime(&provided_classes, class_name, &vec![]).unwrap();

        let result = runtime.invoke_static(class_name, "add", vec![TypedValue::Int(3)]);
        match result {
            TypedValue::Int(x) => assert_eq!(30, x),
            _ => panic!("expected Int result"),
        }
    }

    #[test]
    fn run_inheritance_tests2() {
        // command at time of writing the test would be:
        // $ cargo run test-assets/01-inheritance/X.class,test-assets/01-inheritance/Y.class,test-assets/01-inheritance/Z.class mangrove/test/inheritance/Z
        let provided_classes = vec![
            "test-assets/01-inheritance/X.class",
            "test-assets/01-inheritance/Y.class",
            "test-assets/01-inheritance/Z.class",
        ]
        .join(",");

        let class_name = "mangrove/test/inheritance/Z";

        let mut runtime = crate::startup_runtime(&provided_classes, class_name, &vec![]).unwrap();

        let result = runtime.invoke_static(class_name, "times5", vec![TypedValue::Int(3)]);
        match result {
            TypedValue::Int(x) => assert_eq!(15, x),
            _ => panic!("expected Int result"),
        }
    }

    #[test]
    fn run_invokemain_many_parameters_main() {
        // command at time of writing the test would be:
        // $ cargo run test-assets/02-invoke-main/ManyParameters.class mangrove/test/invokemain/ManyParameters
        let provided_classes = vec!["test-assets/02-invoke-main/ManyParameters.class"].join(",");
        let class_name = "mangrove/test/invokemain/ManyParameters";

        let _runtime = crate::startup_runtime(&provided_classes, class_name, &vec![]).unwrap();
        // we just want to see that the call to main() did not fail
    }

    #[test]
    fn run_invokemain_many_parameters() {
        // command at time of writing the test would be:
        // $ cargo run test-assets/02-invoke-main/ManyParameters.class mangrove/test/invokemain/ManyParameters
        let provided_classes = vec!["test-assets/02-invoke-main/ManyParameters.class"].join(",");
        let class_name = "mangrove/test/invokemain/ManyParameters";

        let mut runtime = crate::startup_runtime(&provided_classes, class_name, &vec![]).unwrap();

        let result = runtime.invoke_static(
            class_name,
            "manyParamsStatic",
            vec![
                TypedValue::Int(7999),
                TypedValue::Long(123456789),
                TypedValue::Char('c'),
            ],
        );
        match result {
            TypedValue::Int(x) => assert_eq!(123465022, x),
            _ => panic!("expected Int result"),
        }
    }

    #[test]
    fn run_invokemain_object_parameters() {
        // command at time of writing the test would be:
        // $ cargo run test-assets/02-invoke-main/ObjectParameters.class mangrove/test/invokemain/ObjectParameters
        let provided_classes = vec!["test-assets/02-invoke-main/ObjectParameters.class"].join(",");
        let class_name = "mangrove/test/invokemain/ObjectParameters";

        let mut runtime = crate::startup_runtime(&provided_classes, class_name, &vec![]).unwrap();

        let result = runtime.invoke_static(class_name, "test", vec![]);
        match result {
            TypedValue::Int(x) => assert_eq!(7, x),
            _ => panic!("expected Int result"),
        }
    }

    #[test]
    fn run_fields_class_hierarchy() {
        // command at time of writing the test would be:
        // $ cargo run test-assets/03-fields/A.class,test-assets/03-fields/B.class,test-assets/03-fields/C.class mangrove/test/fields/C
        let provided_classes = vec![
            "test-assets/03-fields/A.class",
            "test-assets/03-fields/B.class",
            "test-assets/03-fields/C.class",
        ]
        .join(",");
        let class_name = "mangrove/test/fields/C";

        let mut runtime = crate::startup_runtime(&provided_classes, class_name, &vec![]).unwrap();

        let result = runtime.invoke_static(class_name, "test", vec![]);
        match result {
            TypedValue::Int(x) => assert_eq!(12350, x),
            _ => panic!("expected Int result"),
        }
    }

    #[test]
    fn run_fields_array_parameter() {
        // command at time of writing the test would be:
        // $ cargo run test-assets/03-fields/ArrayParameter.class mangrove/test/fields/ArrayParameter
        let provided_classes = vec!["test-assets/03-fields/ArrayParameter.class"].join(",");
        let class_name = "mangrove/test/fields/ArrayParameter";

        let mut runtime = crate::startup_runtime(&provided_classes, class_name, &vec![]).unwrap();

        let result = runtime.invoke_static(class_name, "test", vec![]);
        match result {
            TypedValue::Int(x) => assert_eq!(6, x),
            _ => panic!("expected Int result"),
        }
    }

    #[test]
    fn run_invokemain_main_parameters() {
        // command at time of writing the test would be:
        // $ cargo run test-assets/02-invoke-main/MainParameters.class mangrove/test/invokemain/MainParameters
        let provided_classes = vec!["test-assets/02-invoke-main/MainParameters.class"].join(",");
        let class_name = "mangrove/test/invokemain/MainParameters";

        let mut runtime = crate::startup_runtime(
            &provided_classes,
            class_name,
            &vec![
                "length of 12".to_owned(),
                "short".to_owned(),
                "longer string with length of 31".to_owned(),
            ],
        )
        .unwrap();

        let result = runtime.invoke_static(class_name, "test", vec![]);
        match result {
            TypedValue::Int(x) => assert_eq!(48, x),
            _ => panic!("expected Int result"),
        }
    }

    #[test]
    fn run_hello_world() {
        // actually a test for everything that is needed to run System.out.println("something")

        // command at time of writing the test would be:
        // $ cargo run test-assets/04-hello-world/HelloWorld.class mangrove/test/helloworld/HelloWorld
        let provided_classes = vec!["test-assets/04-hello-world/HelloWorld.class"].join(",");

        let class_name = "mangrove/test/helloworld/HelloWorld";

        let mut runtime = crate::startup_runtime(&provided_classes, class_name, &vec![]).unwrap();

        let result = runtime.invoke_static(
            class_name,
            "add",
            vec![TypedValue::Int(3), TypedValue::Int(8)],
        );
        match result {
            TypedValue::Int(x) => assert_eq!(11, x),
            _ => panic!("expected Int result"),
        }
    }
}
