use std::{io::Result, path::Path};

use crate::class_loader::class_file::ClassFile;

use self::class::Class;

pub mod class;
mod class_file;

pub fn load_class(class_file_path: &Path) -> Result<Class> {
    let class_file = ClassFile::parse(class_file_path)?;

    // println!("{}", &class_file);

    let class = Class::from(class_file);
    println!(" - loaded {}", &class);

    Ok(class)
}

pub fn bytes_to_u32(bytes: &[u8], start: usize) -> u32 {
    u32::from_be_bytes(
        bytes[start..start + 4]
            .try_into()
            .expect("expected sufficiently sized array"),
    )
}

pub fn bytes_to_u16(bytes: &[u8], start: usize) -> u16 {
    u16::from_be_bytes(
        bytes[start..start + 2]
            .try_into()
            .expect("expected sufficiently sized array"),
    )
}
