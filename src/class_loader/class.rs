use std::fmt;

use crate::class_loader::class_file::{AttributeInfo, ClassFile, CpInfo, FieldInfo, MethodInfo};

pub struct Class {
    pub status: ClassStatus,
    pub name: String,
    pub super_class_name: String,
    pub access_flags: Vec<AccessFlag>,
    pub interface_names: Vec<String>,
    pub methods: Vec<Method>,
    pub fields: Vec<Field>,
    pub attributes: Vec<Attribute>,
    pub constant_pool: Vec<ConstantPoolEntry>,
}

#[derive(Debug)]
pub enum ClassStatus {
    Loaded,
    Initialised,
}

pub struct Method {
    pub name: String,
    pub access_flags: Vec<AccessFlag>,
    descriptor: String,
    pub return_type: Type,
    pub parameter_types: Vec<Type>,
    pub attributes: Vec<Attribute>,
    pub code: Option<CodeAttributeEntry>,
    // method_info: MethodInfo,
}

pub struct Field {
    pub name: String,
    pub access_flags: Vec<AccessFlag>,
    descriptor: String,
    pub field_type: Type,
    pub value: TypedValue,
    attributes: Vec<Attribute>,
    // field_info: FieldInfo,
}

pub struct Attribute {
    pub name: String,
    pub value: Vec<u8>,
}

pub struct CodeAttributeEntry {
    pub max_stack: u16,
    pub max_locals: u16,
    pub opcodes: Vec<u8>,
    pub exception_table: Vec<ExceptionTableEntry>,
    pub attributes: Vec<Attribute>,
}

pub struct ExceptionTableEntry {
    pub start_pc: u16,
    pub end_pc: u16,
    pub handler_pc: u16,
    pub catch_type: u16,
}

#[derive(Clone)]
pub enum ConstantPoolEntry {
    Dummy {
        name: String,
    }, // placeholder and unmapped constants
    Class {
        class_name: String,
    },
    Float {
        value: TypedValue,
    },
    Int {
        value: TypedValue,
    },
    Long {
        value: TypedValue,
    },
    Double {
        value: TypedValue,
    },
    String {
        value: String,
    },
    Methodref {
        class_name: String,
        method_name: String,
        return_type: Type,
        parameter_types: Vec<Type>,
    },
    InterfaceMethodref {
        class_name: String,
        method_name: String,
        return_type: Type,
        parameter_types: Vec<Type>,
    },
    Fieldref {
        class_name: String,
        field_name: String,
        field_type: Type,
    },
}

#[derive(PartialEq, Debug, Clone)]
pub enum Type {
    // primitive
    Byte,
    Char,
    Double,
    Float,
    Int,
    Long,
    Short,
    // boolean is somehow special
    Boolean,
    Void,
    // reference types
    Reference(ReferenceType),
}

#[derive(PartialEq, Debug, Clone)]
pub enum ReferenceType {
    Null(Option<Box<ReferenceType>>), // `null` can optionally be typed
    Object(String),
    Array(Box<Type>), //look mum, my first Box
    Interface(String),
}

#[derive(Clone, Debug)]
pub enum TypedValue {
    Byte(i8),
    Char(char),
    Double(f64),
    Float(f32),
    Int(i32),
    Long(i64),
    Short(i16),
    Boolean(bool),
    Void(()),
    Reference(ReferenceTypedValue),
}

#[derive(Clone, Debug)]
pub enum ReferenceTypedValue {
    Null(ReferenceType), // `null` can optionally be typed
    Object(String, usize, usize),
    Array(Type, usize),
    Interface(String, usize, usize),
}

#[derive(PartialEq, Debug)]
pub enum AccessFlag {
    Public,
    Private,
    Protected,
    Static,
    Final,
    Synchronized,
    Bridge,
    Varargs,
    Native,
    Abstract,
    Strict,
    Synthetic,
}

impl fmt::Display for TypedValue {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Byte(v) => write!(f, "Byte {{ value: {} }}", v),
            Self::Char(v) => write!(f, "Char {{ value: {} }}", v),
            Self::Double(v) => write!(f, "Double {{ value: {} }}", v),
            Self::Float(v) => write!(f, "Float {{ value: {} }}", v),
            Self::Int(v) => write!(f, "Int {{ value: {} }}", v),
            Self::Long(v) => write!(f, "Long {{ value: {} }}", v),
            Self::Short(v) => write!(f, "Short {{ value: {} }}", v),
            Self::Boolean(v) => write!(f, "Boolean {{ value: {} }}", v),
            Self::Void(_) => write!(f, "Void {{ }}"),
            Self::Reference(t) => match t {
                ReferenceTypedValue::Null(t) => write!(f, "Reference(Null({:?}))", t),
                ReferenceTypedValue::Object(class_name, class_index, instance_index) => write!(
                    f,
                    "Reference(Object(class_name: {}, class_index: {}, instance_index: {}))",
                    class_name, class_index, instance_index
                ),
                ReferenceTypedValue::Interface(class_name, class_index, instance_index) => write!(
                    f,
                    "Reference(Interface(class_name: {}, class_index: {}, instance_index: {}))",
                    class_name, class_index, instance_index
                ),
                ReferenceTypedValue::Array(array_type, array_instance_index) => {
                    write!(
                        f,
                        "Reference(Array(array_type: {:?}, array_instance_index: {}))",
                        array_type, array_instance_index
                    )
                }
            },
        }
    }
}

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Byte => write!(f, "Byte"),
            Self::Char => write!(f, "Char"),
            Self::Double => write!(f, "Double"),
            Self::Float => write!(f, "Float"),
            Self::Int => write!(f, "Int"),
            Self::Long => write!(f, "Long"),
            Self::Short => write!(f, "Short"),
            Self::Boolean => write!(f, "Boolean"),
            Self::Void => write!(f, "Void"),
            Self::Reference(t) => match t {
                ReferenceType::Null(t) => write!(f, "Reference(Null({:?}))", t),
                ReferenceType::Object(s) => write!(f, "Reference(Object({}))", s),
                ReferenceType::Interface(s) => write!(f, "Reference(Interface({}))", s),
                ReferenceType::Array(t) => write!(f, "Reference(Array({}))", t),
            },
        }
    }
}

impl Type {
    pub fn get_default_typed_value(&self) -> TypedValue {
        match self {
            Self::Byte => TypedValue::Byte(0),
            Self::Char => TypedValue::Char('\u{0000}'),
            Self::Double => TypedValue::Double(0.0),
            Self::Float => TypedValue::Float(0.0),
            Self::Int => TypedValue::Int(0),
            Self::Long => TypedValue::Long(0),
            Self::Short => TypedValue::Short(0),
            Self::Boolean => TypedValue::Boolean(false),
            Self::Void => TypedValue::Void(()),
            Self::Reference(t) => match t {
                ReferenceType::Null(t) => TypedValue::Reference(match t {
                    Some(boxed) => ReferenceTypedValue::Null((**boxed).clone()),
                    None => ReferenceTypedValue::Null(ReferenceType::Null(None)),
                }),
                ReferenceType::Object(_s) => {
                    TypedValue::Reference(ReferenceTypedValue::Null(t.clone()))
                }
                ReferenceType::Interface(_s) => {
                    TypedValue::Reference(ReferenceTypedValue::Null(t.clone()))
                }
                ReferenceType::Array(_t) => {
                    TypedValue::Reference(ReferenceTypedValue::Null(t.clone()))
                }
            },
        }
    }

    pub fn get_specific_int_type_value(&self, value: i32) -> Option<TypedValue> {
        match self {
            Type::Boolean => Some(TypedValue::Boolean(if value == 0 { false } else { true })),
            Type::Byte => Some(TypedValue::Byte(value as i8)),
            Type::Char => Some(TypedValue::Char((value as u8) as char)),
            Type::Short => Some(TypedValue::Short(value as i16)),
            Type::Int => Some(TypedValue::Int(value)),
            _ => None,
        }
    }
}

impl TypedValue {
    pub fn get_type(&self) -> Type {
        match self {
            Self::Byte(_) => Type::Byte,
            Self::Char(_) => Type::Char,
            Self::Double(_) => Type::Double,
            Self::Float(_) => Type::Float,
            Self::Int(_) => Type::Int,
            Self::Long(_) => Type::Long,
            Self::Short(_) => Type::Short,
            Self::Boolean(_) => Type::Boolean,
            Self::Void(_) => Type::Void,
            Self::Reference(t) => match t {
                ReferenceTypedValue::Null(t) => {
                    Type::Reference(ReferenceType::Null(Some(Box::new(t.clone()))))
                }
                ReferenceTypedValue::Object(class_name, _class_index, _instance_index) => {
                    Type::Reference(ReferenceType::Object(class_name.clone()))
                }
                ReferenceTypedValue::Interface(class_name, _class_index, _instance_index) => {
                    Type::Reference(ReferenceType::Interface(class_name.clone()))
                }
                ReferenceTypedValue::Array(t, _array_instance_index, ..) => {
                    Type::Reference(ReferenceType::Array(Box::new(t.clone())))
                }
            },
        }
    }

    pub fn convert_to_int(&self) -> TypedValue {
        match self {
            Self::Boolean(v) => TypedValue::Int(if *v { 1 } else { 0 }),
            Self::Byte(v) => TypedValue::Int(*v as i32),
            Self::Char(v) => TypedValue::Int(*v as i32),
            Self::Short(v) => TypedValue::Int(*v as i32),
            Self::Int(v) => TypedValue::Int(*v),
            _ => panic!("can not convert value to int: {}", self),
        }
    }
}

impl AccessFlag {
    fn parse(access_flags: u16) -> Vec<AccessFlag> {
        //TODO: validate access flags better
        let mut flags = Vec::new();
        if access_flags & 0x1 != 0 {
            flags.push(AccessFlag::Public);
        }
        if access_flags & 0x2 != 0 {
            if flags.contains(&AccessFlag::Public) {
                panic!("unallowed access flag 'Private' when 'Public' is already set");
            }
            flags.push(AccessFlag::Private);
        }
        if access_flags & 0x4 != 0 {
            if flags.contains(&AccessFlag::Public) {
                panic!("unallowed access flag 'Protected' when 'Public' is already set");
            }
            if flags.contains(&AccessFlag::Private) {
                panic!("unallowed access flag 'Protected' when 'Private' is already set");
            }
            flags.push(AccessFlag::Protected);
        }
        if access_flags & 0x8 != 0 {
            flags.push(AccessFlag::Static);
        }
        if access_flags & 0x10 != 0 {
            flags.push(AccessFlag::Final);
        }
        if access_flags & 0x20 != 0 {
            flags.push(AccessFlag::Synchronized);
        }
        if access_flags & 0x40 != 0 {
            flags.push(AccessFlag::Bridge);
        }
        if access_flags & 0x80 != 0 {
            flags.push(AccessFlag::Varargs);
        }
        if access_flags & 0x100 != 0 {
            flags.push(AccessFlag::Native);
        }
        if access_flags & 0x400 != 0 {
            flags.push(AccessFlag::Abstract);
        }
        if access_flags & 0x800 != 0 {
            flags.push(AccessFlag::Strict);
        }
        if access_flags & 0x1000 != 0 {
            flags.push(AccessFlag::Synthetic);
        }
        flags
    }
}

impl fmt::Debug for Class {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}\n", &self)?;
        let access_flags = self
            .access_flags
            .iter()
            .map(|a| format!("{:?}, ", a))
            .collect::<String>();
        write!(f, "  - access_flags: {}\n", &access_flags)?;
        for method in &self.methods {
            write!(f, "  - {}\n", method)?;
        }
        for field in &self.fields {
            write!(f, "  - {}\n", field)?;
        }
        for attribute in &self.attributes {
            write!(f, "  - {}\n", attribute)?;
        }
        write!(f, "\n")
    }
}

impl fmt::Display for Class {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Class {{ name: {}, super_class_name: {}, status: {:?} }}",
            &self.name, self.super_class_name, self.status
        )
    }
}

impl fmt::Display for Method {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let parameter_types = self
            .parameter_types
            .iter()
            .map(|t| format!("{}, ", t))
            .collect::<String>();
        let attributes = self
            .attributes
            .iter()
            .map(|a| format!("{}, ", a.name))
            .collect::<String>();
        let access_flags = self
            .access_flags
            .iter()
            .map(|a| format!("{:?}, ", a))
            .collect::<String>();
        write!(
            f,
            "Method {{ name: {}, access_flags: {}, descriptor: {}, return_type: {}, parameter_types: {}, attributes: {}, code: {} }}",
            &self.name,access_flags, self.descriptor, self.return_type, parameter_types, attributes, match &self.code {
                Some(code) => format!("{}",code),
                None => "None".to_owned()
            }
        )
    }
}

impl fmt::Display for Field {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let attributes = self
            .attributes
            .iter()
            .map(|a| format!("{}, ", a.name))
            .collect::<String>();
        let access_flags = self
            .access_flags
            .iter()
            .map(|a| format!("{:?}, ", a))
            .collect::<String>();
        write!(
            f,
            "Field {{ name: {}, access_flags: {}, descriptor: {}, field_type: {}, attributes: {} }}",
            &self.name, access_flags, self.descriptor, self.field_type, attributes
        )
    }
}

impl fmt::Display for Attribute {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Attribute {{ name: {}, value length: {} }}",
            &self.name,
            self.value.len()
        )
    }
}

impl fmt::Display for CodeAttributeEntry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let attributes = self
            .attributes
            .iter()
            .map(|a| format!("{}, ", a.name))
            .collect::<String>();
        write!(
            f,
            "CodeAttributeEntry {{ max_stack: {}, max_locals: {}, opcodes length: {}, exception table length: {}, attributes: {} }}",
            self.max_stack,
            self.max_locals,
            self.opcodes.len(),
            self.exception_table.len(),
            attributes,
        )
    }
}

impl fmt::Display for ConstantPoolEntry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Class { class_name } => {
                write!(f, "Class {{ class_name: {} }}", class_name)
            }
            Self::Methodref {
                class_name,
                method_name,
                return_type,
                parameter_types,
            } => {
                let parameter_types_str = parameter_types
                    .iter()
                    .map(|t| format!("{}, ", t))
                    .collect::<String>();
                write!(
                    f,
                    "MethodRef {{ class_name: {}, method_name: {}, return_type: {}, parameter_types: {} }}",
                    class_name, method_name, return_type, parameter_types_str
                )
            }
            Self::InterfaceMethodref {
                class_name,
                method_name,
                return_type,
                parameter_types,
            } => {
                let parameter_types_str = parameter_types
                    .iter()
                    .map(|t| format!("{}, ", t))
                    .collect::<String>();
                write!(
                    f,
                    "InterfaceMethodref {{ class_name: {}, method_name: {}, return_type: {}, parameter_types: {} }}",
                    class_name, method_name, return_type, parameter_types_str
                )
            }
            Self::Fieldref {
                class_name,
                field_name,
                field_type,
            } => write!(
                f,
                "Fieldref {{ class_name: {}, field_name: {}, field_type: {} }}",
                class_name, field_name, field_type
            ),
            Self::Float { value } => write!(f, "Float {{ value: {} }}", value),
            Self::Int { value } => write!(f, "Int {{ value: {} }}", value),
            Self::Long { value } => write!(f, "Long {{ value: {} }}", value),
            Self::Double { value } => write!(f, "Double {{ value: {} }}", value),
            Self::String { value } => write!(f, "String {{ value: {} }}", value),
            Self::Dummy { name } => write!(f, "Dummy {{ name: {} }}", name),
        }
    }
}

impl Class {
    pub fn from(class_file: ClassFile) -> Class {
        let class_name =
            ClassFile::get_class_name(&class_file.constant_pool, class_file.this_class).to_owned();
        let super_class_name =
            ClassFile::get_class_name(&class_file.constant_pool, class_file.super_class).to_owned();

        let access_flags = AccessFlag::parse(class_file.access_flags);

        let interface_names =
            Self::map_interfaces(class_file.interfaces, &class_file.constant_pool);
        let methods = Self::map_methods(class_file.methods, &class_file.constant_pool);
        let fields = Self::map_fields(class_file.fields, &class_file.constant_pool);
        let attributes = Self::map_attributes(class_file.attributes, &class_file.constant_pool);

        let constant_pool = Self::map_constant_pool(&class_file.constant_pool);

        Class {
            status: ClassStatus::Loaded,
            name: class_name,
            super_class_name,
            access_flags,
            interface_names,
            methods,
            fields,
            attributes,
            constant_pool,
        }
    }

    fn map_interfaces(interface_indexes: Vec<u16>, constant_pool: &Vec<CpInfo>) -> Vec<String> {
        let mut interface_names = Vec::new();
        for interface_index in interface_indexes {
            let interface_name = ClassFile::get_class_name(constant_pool, interface_index);
            interface_names.push(interface_name.to_owned());
        }
        interface_names
    }

    fn map_methods(
        class_file_methods: Vec<MethodInfo>,
        constant_pool: &Vec<CpInfo>,
    ) -> Vec<Method> {
        let mut methods = Vec::with_capacity(class_file_methods.len());
        for m in class_file_methods {
            let name = ClassFile::get_string_literal(&constant_pool, m.name_index).to_owned();
            let access_flags = AccessFlag::parse(m.access_flags);
            let descriptor =
                ClassFile::get_string_literal(&constant_pool, m.descriptor_index).to_owned();
            let (parameter_types, return_type) = Self::map_method_descriptor(&descriptor);
            let attributes = Self::map_attributes(m.attributes, constant_pool);

            let code = Self::map_code_attribute(&attributes, &access_flags, &name, constant_pool);

            methods.push(Method {
                name,
                access_flags,
                descriptor,
                // method_info: m,
                return_type,
                parameter_types,
                attributes,
                code,
            });
        }
        methods
    }

    fn map_attributes(
        attribute_infos: Vec<AttributeInfo>,
        constant_pool: &Vec<CpInfo>,
    ) -> Vec<Attribute> {
        let mut attributes = Vec::new();
        for a in attribute_infos {
            let attribute_name =
                ClassFile::get_string_literal(&constant_pool, a.attribute_name_index).to_owned();

            attributes.push(Attribute {
                name: attribute_name,
                value: a.info,
            })
        }
        attributes
    }

    fn map_fields(class_file_fields: Vec<FieldInfo>, constant_pool: &Vec<CpInfo>) -> Vec<Field> {
        let mut fields = Vec::with_capacity(class_file_fields.len());
        for f in class_file_fields {
            let name = ClassFile::get_string_literal(&constant_pool, f.name_index).to_owned();
            let access_flags = AccessFlag::parse(f.access_flags);
            let descriptor =
                ClassFile::get_string_literal(&constant_pool, f.descriptor_index).to_owned();
            let field_type = Self::map_field_descriptor(&descriptor);
            let attributes = Self::map_attributes(f.attributes, constant_pool);

            let value = match attributes.iter().find(|a| a.name.eq("ConstantValue")) {
                Some(constant_value) => {
                    let raw_value = &constant_value.value;
                    let constant_value_index = super::bytes_to_u16(&raw_value, 0);
                    let constant_info =
                        ClassFile::get_field_constant(&constant_pool, constant_value_index);
                    match constant_info {
                        CpInfo::Integer { value } => {
                            match field_type.get_specific_int_type_value(*value) {
                                Some(v) => v,
                                None =>panic!("loading field constant failed. Found Integer constant, but can't convert to {}", &field_type),
                            }
                        }
                        CpInfo::Float { value } => TypedValue::Float(*value),
                        CpInfo::Double { value } => TypedValue::Double(*value),
                        CpInfo::Long { value } => TypedValue::Long(*value),
                        CpInfo::String { string_index } => {
                            panic!("loading constant String in field not implemented yet")
                        }
                        _ => panic!(
                            "unexpected constant type found ({}) for field {} with type {}",
                            constant_info, &name, &field_type
                        ),
                    }
                }
                None => field_type.get_default_typed_value(),
            };

            fields.push(Field {
                name,
                access_flags,
                descriptor,
                // field_info: f,
                field_type,
                attributes,
                value,
            });
        }
        fields
    }

    fn map_method_descriptor(descriptor: &str) -> (Vec<Type>, Type) {
        let mut parameter_types = Vec::new();

        let i = match descriptor.find(')') {
            Some(index) => {
                let mut parameter_descriptors = &descriptor[1..index];
                loop {
                    if parameter_descriptors.is_empty() {
                        break;
                    }
                    let (t, i) = Self::map_next_descriptor(parameter_descriptors);
                    parameter_types.push(t);
                    if i >= parameter_descriptors.len() {
                        break;
                    }
                    parameter_descriptors = &parameter_descriptors[i..];
                }
                index + 1
            }
            None => panic!("invalid method descriptor: {}", descriptor),
        };

        let (return_type, _) = Self::map_next_descriptor(&descriptor[i..]);

        (parameter_types, return_type)
    }

    fn map_field_descriptor(descriptor: &str) -> Type {
        Self::map_next_descriptor(descriptor).0
    }

    fn map_next_descriptor(descriptors: &str) -> (Type, usize) {
        match &descriptors[0..1] {
            // base types (including void)
            "B" => return (Type::Byte, 1),
            "C" => return (Type::Char, 1),
            "D" => return (Type::Double, 1),
            "F" => return (Type::Float, 1),
            "I" => return (Type::Int, 1),
            "J" => return (Type::Long, 1),
            "S" => return (Type::Short, 1),
            "Z" => return (Type::Boolean, 1),
            "V" => return (Type::Void, 1),
            // object type
            "L" => match &descriptors[1..].find(';') {
                Some(index) => {
                    let class_name = &descriptors[1..*index + 1];
                    return (
                        Type::Reference(ReferenceType::Object(class_name.to_string())),
                        *index + 2,
                    );
                }
                None => panic!("invalid object notation: {}", &descriptors[1..]),
            },
            // array type
            "[" => {
                let (following_type, index) = Self::map_next_descriptor(&descriptors[1..]);
                return (
                    Type::Reference(ReferenceType::Array(Box::new(following_type))),
                    index + 1,
                );
            }
            _ => panic!("unknown descriptor: {}", descriptors),
        }
    }

    fn map_code_attribute(
        attributes: &Vec<Attribute>,
        access_flags: &Vec<AccessFlag>,
        method_name: &str,
        constant_pool: &Vec<CpInfo>,
    ) -> Option<CodeAttributeEntry> {
        //
        let is_native_or_abstract_method = access_flags.contains(&AccessFlag::Native)
            || access_flags.contains(&AccessFlag::Abstract);
        let is_init_method = method_name.eq("<init>") || method_name.eq("<clinit>");
        let must_not_have_code_attribute = is_native_or_abstract_method && !is_init_method;

        let raw_value = match &attributes.iter().find(|a| a.name.eq("Code")) {
            Some(attribute) => {
                if must_not_have_code_attribute {
                    panic!("found code attribute on method that is native or abstract, and not a class or interface initialisation method");
                }
                &attribute.value
            }
            None => {
                if must_not_have_code_attribute {
                    return None;
                }
                panic!("found no code attribute on method that is neither native nor abstract, or a class or interface initialisation method");
            }
        };

        let mut cursor = 0;
        let max_stack = super::bytes_to_u16(&raw_value, cursor);
        cursor += 2;
        //TODO: continue
        let max_locals = super::bytes_to_u16(&raw_value, cursor);
        cursor += 2;
        let code_length = super::bytes_to_u32(&raw_value, cursor) as usize;
        cursor += 4;
        let opcodes = raw_value[cursor..cursor + code_length].to_vec();
        cursor += code_length;
        let exception_table_length = super::bytes_to_u16(&raw_value, cursor);
        cursor += 2;
        let mut exception_table = Vec::new();
        for _i in 0..exception_table_length {
            let start_pc = super::bytes_to_u16(&raw_value, cursor);
            cursor += 2;
            let end_pc = super::bytes_to_u16(&raw_value, cursor);
            cursor += 2;
            let handler_pc = super::bytes_to_u16(&raw_value, cursor);
            cursor += 2;
            let catch_type = super::bytes_to_u16(&raw_value, cursor);
            cursor += 2;
            exception_table.push(ExceptionTableEntry {
                start_pc,
                end_pc,
                handler_pc,
                catch_type,
            });
        }

        let (_, attribute_infos, _) = ClassFile::read_attributes(&raw_value, cursor);
        let attributes = Self::map_attributes(attribute_infos, &constant_pool);

        Some(CodeAttributeEntry {
            max_stack,
            max_locals,
            opcodes,
            exception_table,
            attributes,
        })
    }

    fn map_constant_pool(constant_pool: &Vec<CpInfo>) -> Vec<ConstantPoolEntry> {
        let mut pool = Vec::with_capacity(constant_pool.len());

        for cp_info in constant_pool {
            let entry = match cp_info {
                CpInfo::Class { name_index } => {
                    let class_name =
                        ClassFile::get_string_literal(constant_pool, *name_index).to_owned();

                    ConstantPoolEntry::Class { class_name }
                }
                CpInfo::Methodref {
                    class_index,
                    name_and_type_index,
                } => {
                    let class_name =
                        ClassFile::get_class_name(constant_pool, *class_index).to_owned();
                    let (method_name, descriptor) =
                        ClassFile::get_name_and_type_info(constant_pool, *name_and_type_index);
                    let method_name = method_name.to_owned();

                    let (parameter_types, return_type) = Self::map_method_descriptor(descriptor);
                    ConstantPoolEntry::Methodref {
                        class_name,
                        method_name,
                        return_type,
                        parameter_types,
                    }
                }
                CpInfo::InterfaceMethodref {
                    class_index,
                    name_and_type_index,
                } => {
                    let class_name =
                        ClassFile::get_class_name(constant_pool, *class_index).to_owned();
                    let (method_name, descriptor) =
                        ClassFile::get_name_and_type_info(constant_pool, *name_and_type_index);
                    let method_name = method_name.to_owned();

                    let (parameter_types, return_type) = Self::map_method_descriptor(descriptor);
                    ConstantPoolEntry::InterfaceMethodref {
                        class_name,
                        method_name,
                        return_type,
                        parameter_types,
                    }
                }
                CpInfo::Fieldref {
                    class_index,
                    name_and_type_index,
                } => {
                    let class_name =
                        ClassFile::get_class_name(constant_pool, *class_index).to_owned();
                    let (field_name, descriptor) =
                        ClassFile::get_name_and_type_info(constant_pool, *name_and_type_index);
                    let field_name = field_name.to_owned();

                    let field_type = Self::map_field_descriptor(descriptor);
                    ConstantPoolEntry::Fieldref {
                        class_name,
                        field_name,
                        field_type,
                    }
                }
                CpInfo::Float { value } => ConstantPoolEntry::Float {
                    value: TypedValue::Float(*value),
                },
                CpInfo::Integer { value } => ConstantPoolEntry::Int {
                    value: TypedValue::Int(*value),
                },
                CpInfo::Long { value } => ConstantPoolEntry::Long {
                    value: TypedValue::Long(*value),
                },
                CpInfo::Double { value } => ConstantPoolEntry::Double {
                    value: TypedValue::Double(*value),
                },
                CpInfo::String { string_index } => {
                    let value = ClassFile::get_string_literal(constant_pool, *string_index);
                    let value = value.to_owned();
                    ConstantPoolEntry::String { value }
                }
                _ => ConstantPoolEntry::Dummy {
                    name: format!("{}", cp_info),
                },
            };
            pool.push(entry);
        }

        pool
    }
}

#[cfg(test)]
mod tests {

    use super::Class;
    use crate::class_loader::class::{ReferenceType, Type};

    #[test]
    fn test_map_method_descriptors() {
        assert_eq!((Vec::new(), Type::Int), Class::map_method_descriptor("()I"));
        assert_eq!(
            (vec![Type::Int, Type::Int], Type::Int),
            Class::map_method_descriptor("(II)I")
        );
        assert_eq!(
            (
                vec![
                    Type::Int,
                    Type::Reference(ReferenceType::Object(String::from("a/b/C"))),
                    Type::Int
                ],
                Type::Reference(ReferenceType::Array(Box::new(Type::Char)))
            ),
            Class::map_method_descriptor("(ILa/b/C;I)[C")
        );
    }

    #[test]
    fn test_map_next_descriptor() {
        assert_eq!((Type::Int, 1), Class::map_next_descriptor("I"));
        assert_eq!((Type::Int, 1), Class::map_next_descriptor("II"));
        assert_eq!(
            (
                Type::Reference(ReferenceType::Object(String::from("java/lang/Object"))),
                18
            ),
            Class::map_next_descriptor("Ljava/lang/Object;")
        );
        assert_eq!(
            (
                Type::Reference(ReferenceType::Array(Box::new(Type::Int))),
                2
            ),
            Class::map_next_descriptor("[I")
        );
        assert_eq!(
            (
                Type::Reference(ReferenceType::Array(Box::new(Type::Reference(
                    ReferenceType::Array(Box::new(Type::Reference(ReferenceType::Array(
                        Box::new(Type::Int)
                    ))))
                )))),
                4
            ),
            Class::map_next_descriptor("[[[I")
        );
        assert_eq!(
            (
                Type::Reference(ReferenceType::Array(Box::new(Type::Reference(
                    ReferenceType::Object(String::from("a/b/c/D"))
                )))),
                10
            ),
            Class::map_next_descriptor("[La/b/c/D;")
        );
    }
}
