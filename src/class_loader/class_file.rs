use core::panic;
use std::{fmt, io::Result, path::Path};

pub struct ClassFile {
    magic: u32,
    minor_version: u16,
    major_version: u16,
    constant_pool_count: u16,
    pub constant_pool: Vec<CpInfo>,
    pub access_flags: u16,
    pub this_class: u16,
    pub super_class: u16,
    interfaces_count: u16,
    pub interfaces: Vec<u16>,
    fields_count: u16,
    pub fields: Vec<FieldInfo>,
    methods_count: u16,
    pub methods: Vec<MethodInfo>,
    attributes_count: u16,
    pub attributes: Vec<AttributeInfo>,
}

impl fmt::Display for ClassFile {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "ClassFile {{\n")?;
        write!(f, "  magic number: {} / {:x}\n", self.magic, self.magic)?;
        write!(f, "  minor version: {}\n", self.minor_version)?;
        write!(f, "  major version: {}\n", self.major_version)?;
        write!(f, "  constant pool count: {}\n", self.constant_pool_count)?;
        write!(f, "  constant pool:\n")?;
        for cp in &self.constant_pool {
            write!(f, "  - {}\n", cp)?
        }
        write!(f, "  access_flags: {}\n", self.access_flags)?;
        write!(f, "  this_class: {}\n", self.this_class)?;
        write!(f, "  super_class: {}\n", self.super_class)?;
        write!(f, "  interfaces count: {}\n", self.interfaces_count)?;
        write!(f, "  interfaces:\n")?;
        for i in &self.interfaces {
            write!(f, "  - {}\n", i)?
        }
        write!(f, "  fields count: {}\n", self.fields_count)?;
        write!(f, "  fields:\n")?;
        for field in &self.fields {
            write!(f, "  - {}\n", field)?
        }
        write!(f, "  methods count: {}\n", self.methods_count)?;
        write!(f, "  methods:\n")?;
        for m in &self.methods {
            write!(f, "  - {}\n", m)?
        }
        write!(f, "  attributes count: {}\n", self.attributes_count)?;
        for a in &self.attributes {
            write!(f, "  - {}\n", a)?
        }
        write!(f, "}}\n")
    }
}

pub enum CpInfo {
    Utf8 {
        value: String,
    },
    Integer {
        value: i32,
    },
    Float {
        value: f32,
    },
    Long {
        value: i64,
    },
    Double {
        value: f64,
    },
    Class {
        name_index: u16,
    },
    String {
        string_index: u16,
    },
    Fieldref {
        class_index: u16,
        name_and_type_index: u16,
    },
    Methodref {
        class_index: u16,
        name_and_type_index: u16,
    },
    InterfaceMethodref {
        class_index: u16,
        name_and_type_index: u16,
    },
    NameAndType {
        name_index: u16,
        descriptor_index: u16,
    },
    MethodHandle {
        reference_kind: u8,
        reference_index: u16,
    },
    MethodType {
        descriptor_index: u16,
    },
    Dynamic {
        bootstrap_method_attr_index: u16,
        name_and_type_index: u16,
    },
    InvokeDynamic {
        bootstrap_method_attr_index: u16,
        name_and_type_index: u16,
    },
    Module {
        name_index: u16,
    },
    Package {
        name_index: u16,
    },
    Dummy, // used as placeholder
}

impl fmt::Display for CpInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Utf8 { value } => write!(f, "Utf8 {{ value: \"{}\" }}", value),
            Self::Integer { value } => write!(f, "Integer {{ value: {} }}", value),
            Self::Float { value } => write!(f, "Float {{ value: {} }}", value),
            Self::Long { value } => write!(f, "Long {{ value: {} }}", value),
            Self::Double { value } => write!(f, "Double {{ value: {} }}", value),
            Self::Class { name_index: _ } => write!(f, "Class"),
            Self::String { string_index: _ } => write!(f, "String"),
            Self::Fieldref {
                class_index: _,
                name_and_type_index: _,
            } => write!(f, "Fieldref"),
            Self::Methodref {
                class_index: _,
                name_and_type_index: _,
            } => write!(f, "Methodref"),
            Self::InterfaceMethodref {
                class_index: _,
                name_and_type_index: _,
            } => write!(f, "InterfaceMethodref"),
            Self::NameAndType {
                name_index: _,
                descriptor_index: _,
            } => write!(f, "NameAndType"),
            Self::MethodHandle {
                reference_kind: _,
                reference_index: _,
            } => write!(f, "MethodHandle"),
            Self::MethodType {
                descriptor_index: _,
            } => write!(f, "MethodType"),
            Self::Dynamic {
                bootstrap_method_attr_index: _,
                name_and_type_index: _,
            } => write!(f, "Dynamic"),
            Self::InvokeDynamic {
                bootstrap_method_attr_index: _,
                name_and_type_index: _,
            } => write!(f, "InvokeDynamic"),
            Self::Module { name_index: _ } => write!(f, "Module"),
            Self::Package { name_index: _ } => write!(f, "Package"),
            Self::Dummy => write!(f, "Dummy / Placeholder"),
        }
    }
}

pub struct FieldInfo {
    pub access_flags: u16,
    pub name_index: u16,
    pub descriptor_index: u16,
    attributes_count: u16,
    pub attributes: Vec<AttributeInfo>,
}

impl fmt::Display for FieldInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "FieldInfo {{ access_flags: {}, name_index: {}, descriptor_index: {}, attributes_count: {} }}",
            self.access_flags, self.name_index, self.descriptor_index, self.attributes_count
        )?;
        for a in &self.attributes {
            write!(f, "  - {}", a)?
        }
        write!(f, "")
    }
}

pub struct MethodInfo {
    pub access_flags: u16,
    pub name_index: u16,
    pub descriptor_index: u16,
    attributes_count: u16,
    pub attributes: Vec<AttributeInfo>,
}

impl fmt::Display for MethodInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "MethodInfo {{ access_flags: {}, name_index: {}, descriptor_index: {}, attributes_count: {} }}",
            self.access_flags, self.name_index, self.descriptor_index, self.attributes_count
        )?;
        for a in &self.attributes {
            write!(f, "  - {}", a)?
        }
        write!(f, "")
    }
}

pub struct AttributeInfo {
    pub attribute_name_index: u16,
    attribute_length: u32,
    pub info: Vec<u8>,
}

impl fmt::Display for AttributeInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "AttributeInfo {{ attribute_name_index: {}, attribute_length: {}, info: {:?} }}",
            self.attribute_name_index, self.attribute_length, self.info
        )
    }
}

impl ClassFile {
    pub fn get_string_literal(constant_pool: &Vec<CpInfo>, index: u16) -> &str {
        let constant_index = index as usize;
        match constant_pool.get(constant_index) {
            Some(constant) => match constant {
                CpInfo::Utf8 { value } => value,
                _ => panic!("not a string literal"),
            },
            None => panic!("invalid constant index request"),
        }
    }

    pub fn get_name_and_type_info(constant_pool: &Vec<CpInfo>, index: u16) -> (&str, &str) {
        let constant_index = index as usize;
        match constant_pool.get(constant_index) {
            Some(constant) => match constant {
                CpInfo::NameAndType {
                    name_index,
                    descriptor_index,
                } => (
                    Self::get_string_literal(constant_pool, *name_index),
                    Self::get_string_literal(constant_pool, *descriptor_index),
                ),
                _ => panic!("not a string literal"),
            },
            None => panic!("invalid constant index request"),
        }
    }

    pub fn get_class_name(constant_pool: &Vec<CpInfo>, index: u16) -> &str {
        if index == 0 {
            return "java/lang/Object";
        }
        let constant_index = index as usize;
        match constant_pool.get(constant_index) {
            Some(constant) => match constant {
                CpInfo::Class { name_index } => {
                    Self::get_string_literal(constant_pool, *name_index)
                }
                _ => panic!("not a string literal at index {}: {}", index, constant),
            },
            None => panic!("invalid constant index request"),
        }
    }

    pub fn get_field_constant(constant_pool: &Vec<CpInfo>, constant_value_index: u16) -> &CpInfo {
        let index = constant_value_index as usize;
        match constant_pool.get(index) {
            Some(constant) => constant,
            None => panic!("invalid constant index request"),
        }
    }

    pub fn parse(file: &Path) -> Result<ClassFile> {
        let file = file.canonicalize()?;
        // println!("reading file {:?}", file);
        let file_content = std::fs::read(file)?;

        let mut cursor = 0;
        let magic = super::bytes_to_u32(&file_content, cursor);
        cursor += 4;
        assert_eq!(magic, 0xcafebabe, "magic number needs to be 0xCAFEBABE");
        let minor_version = super::bytes_to_u16(&file_content, cursor);
        cursor += 2;
        let major_version = super::bytes_to_u16(&file_content, cursor);
        cursor += 2;
        assert!(
            major_version >= 45 && major_version <= 61,
            "Java version up to 17 supported. Illegal major_version: {}",
            major_version
        );
        assert!(
            major_version >= 56 && (minor_version == 0 || minor_version == 65535),
            "illegal major/minor version combination. major_version: {}, minor_version: {}",
            major_version,
            minor_version
        );
        let (constant_pool_count, constant_pool, cursor_after_constants) =
            Self::read_constant_pool(&file_content, cursor);
        cursor = cursor_after_constants;
        let access_flags = super::bytes_to_u16(&file_content, cursor);
        cursor += 2;
        let this_class = super::bytes_to_u16(&file_content, cursor);
        cursor += 2;
        let super_class = super::bytes_to_u16(&file_content, cursor);
        cursor += 2;
        let interfaces_count = super::bytes_to_u16(&file_content, cursor);
        cursor += 2;
        let mut interfaces = Vec::with_capacity(interfaces_count as usize);
        for _i in 0..interfaces_count {
            let interface = super::bytes_to_u16(&file_content, cursor);
            cursor += 2;
            interfaces.push(interface);
        }
        let fields_count = super::bytes_to_u16(&file_content, cursor);
        cursor += 2;
        let mut fields = Vec::with_capacity(fields_count as usize);
        for _i in 0..fields_count {
            let field_access_flags = super::bytes_to_u16(&file_content, cursor);
            cursor += 2;
            let field_name_index = super::bytes_to_u16(&file_content, cursor);
            cursor += 2;
            let field_descriptor_index = super::bytes_to_u16(&file_content, cursor);
            cursor += 2;
            let (field_attributes_count, field_attributes, cursor_after_attributes) =
                Self::read_attributes(&file_content, cursor);
            cursor = cursor_after_attributes;

            fields.push(FieldInfo {
                access_flags: field_access_flags,
                name_index: field_name_index,
                descriptor_index: field_descriptor_index,
                attributes_count: field_attributes_count,
                attributes: field_attributes,
            })
        }
        let methods_count = super::bytes_to_u16(&file_content, cursor);
        cursor += 2;
        let mut methods = Vec::with_capacity(methods_count as usize);
        for _i in 0..methods_count {
            let method_access_flags = super::bytes_to_u16(&file_content, cursor);
            cursor += 2;
            let method_name_index = super::bytes_to_u16(&file_content, cursor);
            cursor += 2;
            let method_descriptor_index = super::bytes_to_u16(&file_content, cursor);
            cursor += 2;
            let (method_attributes_count, method_attributes, cursor_after_attributes) =
                Self::read_attributes(&file_content, cursor);
            cursor = cursor_after_attributes;

            methods.push(MethodInfo {
                access_flags: method_access_flags,
                name_index: method_name_index,
                descriptor_index: method_descriptor_index,
                attributes_count: method_attributes_count,
                attributes: method_attributes,
            })
        }
        let (attributes_count, attributes, cursor_after_attributes) =
            Self::read_attributes(&file_content, cursor);
        cursor = cursor_after_attributes;

        if cursor != file_content.len() {
            panic!(
                "error reading class file, not everything is read! Cursor at end: {}, file size: {}",
                cursor,
                file_content.len()
            );
        }

        Ok(ClassFile {
            magic,
            minor_version,
            major_version,
            constant_pool_count,
            constant_pool,
            access_flags,
            this_class,
            super_class,
            interfaces_count,
            interfaces,
            fields_count,
            fields,
            methods_count,
            methods,
            attributes_count,
            attributes,
        })
    }

    fn modified_utf8_to_string(bytes: &Vec<u8>) -> String {
        //TODO: implement "proper" parsing of "modified utf8" to standard utf8
        String::from_utf8(bytes.clone()).unwrap()
    }

    fn read_constant_pool(file_content: &Vec<u8>, index: usize) -> (u16, Vec<CpInfo>, usize) {
        let mut cursor = index;

        let constant_pool_count = super::bytes_to_u16(&file_content, cursor);
        cursor += 2;

        let mut cp_infos = Vec::with_capacity(constant_pool_count as usize);

        cp_infos.push(CpInfo::Dummy); // indices start at 1, so we add a dummy here

        let mut cp_index = 1;
        while cp_index < constant_pool_count {
            let tag = file_content[cursor];
            cursor += 1;
            let cp_info = match tag {
                1 => {
                    let length = super::bytes_to_u16(&file_content, cursor);
                    cursor += 2;
                    let start_index = cursor;
                    cursor += length as usize;
                    let bytes = file_content[start_index..cursor].to_vec();
                    let value = Self::modified_utf8_to_string(&bytes);

                    CpInfo::Utf8 { value }
                }
                3 => {
                    let start_index = cursor;
                    cursor += 4;
                    let bytes = file_content[start_index..cursor].try_into().unwrap();
                    let value = i32::from_be_bytes(bytes);

                    CpInfo::Integer { value }
                }
                4 => {
                    let start_index = cursor;
                    cursor += 4;
                    let bytes = file_content[start_index..cursor].try_into().unwrap();
                    let value = f32::from_be_bytes(bytes);

                    CpInfo::Float { value }
                }
                5 => {
                    let start_index = cursor;
                    cursor += 8;

                    let bytes = file_content[start_index..cursor].try_into().unwrap();
                    let value = i64::from_be_bytes(bytes);

                    CpInfo::Long { value }
                }
                6 => {
                    let start_index = cursor;
                    cursor += 8;

                    let bytes = file_content[start_index..cursor].try_into().unwrap();
                    let value = f64::from_be_bytes(bytes);

                    CpInfo::Double { value }
                }
                7 => {
                    let start_index = cursor;
                    cursor += 2;

                    CpInfo::Class {
                        name_index: super::bytes_to_u16(&file_content, start_index),
                    }
                }
                8 => {
                    let start_index = cursor;
                    cursor += 2;

                    CpInfo::String {
                        string_index: super::bytes_to_u16(&file_content, start_index),
                    }
                }
                9 => {
                    let start_index = cursor;
                    cursor += 2;
                    let mid_index = cursor;
                    cursor += 2;

                    CpInfo::Fieldref {
                        class_index: super::bytes_to_u16(&file_content, start_index),
                        name_and_type_index: super::bytes_to_u16(&file_content, mid_index),
                    }
                }
                10 => {
                    let start_index = cursor;
                    cursor += 2;
                    let mid_index = cursor;
                    cursor += 2;

                    let class_index = super::bytes_to_u16(&file_content, start_index);
                    let name_and_type_index = super::bytes_to_u16(&file_content, mid_index);

                    CpInfo::Methodref {
                        class_index,
                        name_and_type_index,
                    }
                }
                11 => {
                    let start_index = cursor;
                    cursor += 2;
                    let mid_index = cursor;
                    cursor += 2;

                    CpInfo::InterfaceMethodref {
                        class_index: super::bytes_to_u16(&file_content, start_index),
                        name_and_type_index: super::bytes_to_u16(&file_content, mid_index),
                    }
                }
                12 => {
                    let start_index = cursor;
                    cursor += 2;
                    let mid_index = cursor;
                    cursor += 2;

                    CpInfo::NameAndType {
                        name_index: super::bytes_to_u16(&file_content, start_index),
                        descriptor_index: super::bytes_to_u16(&file_content, mid_index),
                    }
                }
                15 => {
                    let start_index = cursor;
                    cursor += 1;
                    let mid_index = cursor;
                    cursor += 2;

                    CpInfo::MethodHandle {
                        reference_kind: file_content[start_index],
                        reference_index: super::bytes_to_u16(&file_content, mid_index),
                    }
                }
                16 => {
                    let start_index = cursor;
                    cursor += 2;
                    CpInfo::MethodType {
                        descriptor_index: super::bytes_to_u16(&file_content, start_index),
                    }
                }
                17 => {
                    let start_index = cursor;
                    cursor += 2;
                    let mid_index = cursor;
                    cursor += 2;

                    CpInfo::Dynamic {
                        bootstrap_method_attr_index: super::bytes_to_u16(
                            &file_content,
                            start_index,
                        ),
                        name_and_type_index: super::bytes_to_u16(&file_content, mid_index),
                    }
                }
                18 => {
                    let start_index = cursor;
                    cursor += 2;
                    let mid_index = cursor;
                    cursor += 2;

                    CpInfo::InvokeDynamic {
                        bootstrap_method_attr_index: super::bytes_to_u16(
                            &file_content,
                            start_index,
                        ),
                        name_and_type_index: super::bytes_to_u16(&file_content, mid_index),
                    }
                }
                19 => {
                    let start_index = cursor;
                    cursor += 2;

                    CpInfo::Module {
                        name_index: super::bytes_to_u16(&file_content, start_index),
                    }
                }
                20 => {
                    let start_index = cursor;
                    cursor += 2;

                    CpInfo::Package {
                        name_index: super::bytes_to_u16(&file_content, start_index),
                    }
                }
                _ => panic!("unknown tag: {}", tag),
            };

            // Double and Long take up two slots in the constant_pool
            let add_dup = match cp_info {
                CpInfo::Double { value } => Some(CpInfo::Double { value }),
                CpInfo::Long { value } => Some(CpInfo::Long { value }),
                _ => None,
            };
            match add_dup {
                Some(dup) => {
                    cp_index += 1;
                    cp_infos.push(dup);
                }
                None => (),
            }
            cp_infos.push(cp_info);
            cp_index += 1;
        }

        (constant_pool_count, cp_infos, cursor)
    }

    pub fn read_attributes(
        file_content: &Vec<u8>,
        start_cursor: usize,
    ) -> (u16, Vec<AttributeInfo>, usize) {
        let mut cursor = start_cursor;
        let attributes_count = super::bytes_to_u16(&file_content, cursor);
        cursor += 2;
        let mut attributes = Vec::with_capacity(attributes_count as usize);
        for _j in 0..attributes_count {
            let attribute_name_index = super::bytes_to_u16(&file_content, cursor);
            cursor += 2;
            let attribute_length = super::bytes_to_u32(&file_content, cursor);
            cursor += 4;
            let info = file_content[cursor..cursor + attribute_length as usize].to_vec();
            cursor += attribute_length as usize;
            attributes.push(AttributeInfo {
                attribute_name_index,
                attribute_length,
                info,
            })
        }
        (attributes_count, attributes, cursor)
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use crate::class_loader::class_file::ClassFile;

    #[test]
    fn parse_hello_world_class_file() {
        let file = Path::new("test-assets/04-hello-world/HelloWorld.class");
        let class_file = match ClassFile::parse(&file) {
            Ok(class_file) => class_file,
            Err(err) => panic!("unable to read test class file: {}", err),
        };
        assert_eq!(61, class_file.major_version, "expected java 17 class file");
        assert_eq!(
            53, class_file.constant_pool_count,
            "wrong number of constants"
        );
    }

    #[test]
    fn parse_addition_class_file() {
        let file = Path::new("test-assets/00-start/Addition.class");
        let class_file = match ClassFile::parse(&file) {
            Ok(class_file) => class_file,
            Err(err) => panic!("unable to read test class file: {}", err),
        };
        assert_eq!(61, class_file.major_version, "expected java 17 class file");
        assert_eq!(
            17, class_file.constant_pool_count,
            "wrong number of constants"
        );
    }
}
