use crate::{
    class_loader::{
        self,
        class::{
            AccessFlag, Class, ClassStatus, ReferenceType, ReferenceTypedValue, Type, TypedValue,
        },
    },
    runtime::data_areas::{ArrayInstance, InstanceField},
};
use std::{fmt, io::Result, path::Path};

use self::{
    data_areas::{Heap, Instance, VmStack, VmThread},
    native::Native,
};

mod data_areas;
mod interpreter;
mod native;

pub struct Runtime {
    vm_threads: Vec<VmThread>,
    heap: Heap,
    native: Native,
}

impl fmt::Display for Runtime {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let vm_threads: String = self
            .vm_threads
            .iter()
            .map(|vmt| format!("{}, ", vmt))
            .collect();
        write!(
            f,
            "Runtime {{  vm_threads: {}, heap: {} }}",
            vm_threads, self.heap
        )
    }
}

impl Runtime {
    pub fn new() -> Runtime {
        Runtime {
            vm_threads: vec![VmThread {
                vm_stack: VmStack { frames: vec![] },
                program_counter: 0,
            }],
            heap: Heap {
                method_area: Vec::new(),
                instances: Vec::new(),
                array_instances: Vec::new(),
            },
            native: Native::bootstrap(),
        }
    }

    pub fn load(&mut self, path: &Path) -> Result<usize> {
        let class = match Self::get_extension_as_str(&path) {
            Some(extension) => match extension {
                "class" => class_loader::load_class(path)?,
                "jar" => panic!("loading of jar files not implemented yet"),
                _ => panic!("unsupported file type: {}", extension),
            },
            None => panic!("unknown file type: {}", &path.display()),
        };

        let index = match self.heap.get_class_by_name(&class.name) {
            Some((_index, class)) => {
                panic!("attempted to load already loaded class: {}", &class.name)
            } //TODO: maybe only print, instead of panic?
            None => {
                self.check_native_methods(&class);
                self.heap.method_area.push(class);
                self.heap.method_area.len() - 1
            }
        };

        //TODO: when to invoke <clinit> method of class?

        Ok(index)
    }

    pub fn init_all_classes(&mut self) {
        let class_indexes_with_static_initialiser: Vec<usize> = self
            .heap
            .method_area
            .iter()
            .enumerate()
            .filter(|(_index, class)| class.methods.iter().any(|m| m.name.eq("<clinit>")))
            .map(|(index, _class)| index)
            .collect();
        for class_index in class_indexes_with_static_initialiser {
            let super_class_indexes = self.get_class_hierarchy(class_index);
            println!("+ classes to load: {:?}", super_class_indexes);

            for class_index_to_load_before in super_class_indexes {
                println!("  + will load superclass: {}", class_index_to_load_before);
            }

            println!("  + will load class: {}", class_index);
            self.init_class(class_index);
        }
    }

    fn get_class_hierarchy(&self, class_index: usize) -> Vec<usize> {
        let mut class_indexes_to_load = Vec::new();
        let class = self.heap.get_class_by_index(class_index);
        if !class.name.eq(&class.super_class_name) {
            // load superclass first
            match self.heap.get_class_index_by_name(&class.super_class_name) {
                Some(super_class_index) => {
                    class_indexes_to_load.append(&mut self.get_class_hierarchy(super_class_index));
                }
                None => (),
            }
        } else {
            class_indexes_to_load.push(class_index);
        }
        class_indexes_to_load
    }

    fn init_class(&mut self, class_index: usize) {
        //TODO: could be done more elegantly
        let mut run_string_extra_action = false;
        let mut run_system_extra_action = false;
        let initialised = {
            let class = self.heap.get_class_by_index_mut(class_index);
            match class.status {
                ClassStatus::Loaded => {
                    let class_name = class.name.clone();
                    let init_result =
                        interpreter::interpret(self, class_index, "<clinit>", Vec::new(), None);
                    if class_name.eq("java/lang/String") {
                        run_string_extra_action = true;
                    }
                    if class_name.eq("java/lang/System") {
                        run_system_extra_action = true;
                    }
                    match init_result {
                        TypedValue::Void(..) => true,
                        _ => panic!(
                            "unexpected result type of class initialisation: {}",
                            init_result
                        ),
                    }
                }
                _ => {
                    print!("+ ignoring init of already loaded class: {}", class.name);
                    false
                }
            }
        };
        if run_string_extra_action {
            // the borrow checker can be mean at times :)
            // (or I'm too inexperienced. Probably both.)
            println!("    ++ extra action for java/lang/String: set COMPACT_STRINGS to false");
            let class = self.heap.get_class_by_index_mut(class_index);
            let field = class
                .fields
                .iter_mut()
                .find(|f| f.name.eq("COMPACT_STRINGS"))
                .expect("expected field COMPACT_STRINGS in String.class");
            field.value = TypedValue::Boolean(false);
        }
        if run_system_extra_action {
            println!(
                "    ++ extra action for java/lang/System: run initPhase1 (at least parts of it)"
            );
            // currently it's not feasible to run the complete methods, since there are so many internal things going on.
            // instead, we will try to initialise the standard output stream only

            // 1) set lineSeparator to '\n'
            {
                let line_separator = self.create_string_instance("\n");
                let class = self.heap.get_class_by_index_mut(class_index);
                let field_line_separator = class
                    .fields
                    .iter_mut()
                    .find(|f| f.name.eq("lineSeparator"))
                    .expect("expected field lineSeparator in System.class");
                field_line_separator.value = line_separator;
            }
            // 2) init Properties holder
            {
                let properties_class_name = "java/util/Properties";
                let properties_class_index =
                    match self.heap.get_class_index_by_name(properties_class_name) {
                        Some(index) => index,
                        None => panic!("could not find loaded class: {}", properties_class_name),
                    };
                let properties_instance_index =
                    self.create_instance_uninitialised(properties_class_index);
                interpreter::interpret(
                    self,
                    properties_class_index,
                    "<init>",
                    Vec::new(),
                    Some(properties_instance_index),
                );

                let class = self.heap.get_class_by_index_mut(class_index);
                let field_properties = class
                    .fields
                    .iter_mut()
                    .find(|f| f.name.eq("props"))
                    .expect("expected field props in System.class");
                field_properties.value = TypedValue::Reference(ReferenceTypedValue::Object(
                    properties_class_name.to_owned(),
                    properties_class_index,
                    properties_instance_index,
                ));
            }
            // 3) create FileOutputStream
            // 3.1) get field FileDescriptor.out
            let file_descriptor_out = match self.heap.get_class_by_name("java/io/FileDescriptor") {
                Some((_class_index, class)) => class
                    .fields
                    .iter()
                    .find(|f| f.name.eq("in"))
                    .expect("expected field in in FileDescriptor.class")
                    .value
                    .clone(),
                None => panic!(
                    "trying to load class with name: {} but could not find class",
                    "java/io/FileDescriptor"
                ),
            };
            // 4) invoke newPrintStream with FileOutputStream (and null encoding for the time being)
            let new_print_stream_parameters = vec![
                file_descriptor_out,
                TypedValue::Reference(ReferenceTypedValue::Null(ReferenceType::Null(None))),
            ];
            let print_stream_reference = self.invoke_static(
                "java/lang/System",
                "newPrintStream",
                new_print_stream_parameters,
            );
            // 5) pass result to setOut0
            self.invoke_static("java/lang/System", "setOut0", vec![print_stream_reference]);
        }
        if initialised {
            self.heap.get_class_by_index_mut(class_index).status = ClassStatus::Initialised;
        }
    }

    fn get_extension_as_str(path: &Path) -> Option<&str> {
        match path.extension() {
            Some(extension) => extension.to_str(),
            None => None,
        }
    }

    pub fn invoke_main(&mut self, class_name: &str, arguments: &[String]) {
        let mut parameters = Vec::new();
        for arg in arguments {
            let string_parameter = self.create_string_instance(arg);
            parameters.push(string_parameter);
        }
        let array_type = Type::Reference(ReferenceType::Object("java/lang/String".to_owned()));
        let array_instance_index = self.create_array_instance(array_type.clone(), parameters);
        let main_parameters = vec![TypedValue::Reference(ReferenceTypedValue::Array(
            array_type,
            array_instance_index,
        ))];

        let class_index = match self.heap.get_class_index_by_name(class_name) {
            Some(class_index) => class_index,
            None => panic!("could not find class for class name: {}", class_name),
        };
        let method_name = "main";

        let return_value =
            interpreter::interpret(self, class_index, method_name, main_parameters, None);
        println!("method invocation returned: {}", &return_value);
    }

    pub fn invoke_static(
        &mut self,
        class_name: &str,
        method_name: &str,
        parameters: Vec<TypedValue>,
    ) -> TypedValue {
        let class_index = match self.heap.get_class_index_by_name(class_name) {
            Some(class_index) => class_index,
            None => panic!("could not find class for class name: {}", class_name),
        };

        interpreter::interpret(self, class_index, method_name, parameters, None)
    }

    pub fn create_instance_uninitialised(&mut self, class_index: usize) -> usize {
        let class = self.heap.get_class_by_index(class_index);

        println!(" -> creating instance for class: {}", class.name);

        let field_values = self.find_all_fields(class);

        let instance_index = self.heap.instances.len();
        let instance = Instance {
            reference: ReferenceTypedValue::Object(class.name.clone(), class_index, instance_index),
            class_index,
            field_values,
        };
        self.heap.instances.push(instance);

        instance_index
    }

    fn create_array_instance(&mut self, array_type: Type, components: Vec<TypedValue>) -> usize {
        let array_instance_index = self.heap.array_instances.len();
        let array_instance = ArrayInstance {
            components,
            reference: ReferenceTypedValue::Array(array_type, array_instance_index),
        };
        self.heap.array_instances.push(array_instance);
        array_instance_index
    }

    fn create_string_instance(&mut self, value: &str) -> TypedValue {
        let class_name = "java/lang/String";
        println!("creating {} instance with value: {}", class_name, value);
        match self.heap.get_class_index_by_name(class_name) {
            Some(class_index) => {
                let instance_index = self.create_instance_uninitialised(class_index);
                //TODO: init with actual value
                //TODO: could be (should be) UTF-16

                let chars = value.chars().map(|c| TypedValue::Char(c)).collect();
                let array_instance_index = self.create_array_instance(Type::Char, chars);

                let parameter = vec![TypedValue::Reference(ReferenceTypedValue::Array(
                    Type::Char,
                    array_instance_index,
                ))];
                interpreter::interpret(
                    self,
                    class_index,
                    "<init>",
                    parameter,
                    Some(instance_index),
                );
                TypedValue::Reference(ReferenceTypedValue::Object(
                    class_name.to_owned(),
                    class_index,
                    instance_index,
                ))
            }
            None => panic!("could not find class: {}", class_name),
        }
    }

    fn create_jvm_class_instance(&mut self, generic_class_index: usize) -> (usize, usize) {
        let class_class_name = "java/lang/Class";
        let generic_class_instance_index = self.create_instance_uninitialised(generic_class_index);
        let generic_class_name = self
            .heap
            .get_class_by_index(generic_class_index)
            .name
            .clone();

        let string_instance = self.create_string_instance(&generic_class_name);
        println!(" -> creating Class<?> instance for: {}", generic_class_name);

        // let class_loader_name = "java/lang/ClassLoader";

        let (field_values, class_class_index) = match self.heap.get_class_by_name(class_class_name)
        {
            Some((class_class_index, class_class)) => {
                //TODO: use fields properly, that's just a prototype
                let mut field_values = Vec::new();
                for field in &class_class.fields {
                    let value = if field.name.eq("name") {
                        // push a java String with the name, in descriptor format
                        string_instance.clone()
                    } else if field.name.eq("classData") {
                        // push... hmm, an instance? Hard to do for interfaces and abstract classes, strange!
                        TypedValue::Reference(ReferenceTypedValue::Object(
                            generic_class_name.clone(),
                            generic_class_index,
                            generic_class_instance_index,
                        ))
                    // } else if field.name.eq("classLoader") {
                    //     // oof, new we also need a ClassLoader representation
                    //     TypedValue::Reference(ReferenceTypedValue::Object(
                    //         class_loader_name.to_owned(),
                    //         class_loader_index,
                    //         class_loader_instance_index,
                    // ))
                    } else {
                        field.field_type.get_default_typed_value()
                    };
                    field_values.push(InstanceField {
                        name: field.name.clone(),
                        value,
                    });
                }
                (field_values, class_class_index)
            }
            None => panic!("could not find class with name: {}", class_class_name),
        };

        let instance_index = self.heap.instances.len();
        let instance = Instance {
            reference: ReferenceTypedValue::Object(
                class_class_name.to_owned(),
                generic_class_index,
                instance_index,
            ),
            class_index: generic_class_index,
            field_values,
        };
        self.heap.instances.push(instance);

        (class_class_index, instance_index)
    }

    fn check_native_methods(&self, class: &Class) {
        let class_name = &class.name;
        class.methods.iter()
        .filter(|method|method.access_flags.contains(&AccessFlag::Native))
        .filter(|native_method| !self.native.has_native_method_implementation(class_name, &native_method.name))
        .for_each(|unimplemented_native_method| println!("WARNING - loaded class {} with native method {}, but found no native implementation", class_name, &unimplemented_native_method.name));
    }

    fn find_all_fields(&self, class: &Class) -> Vec<InstanceField> {
        let mut field_values = Vec::new();
        // 1) look in mentioned class
        field_values.append(&mut self.find_fields_for_class(class));
        // 2) look recursively in direct superinterfaces
        field_values.append(&mut self.find_fields_for_interfaces(class));
        // 3) look recursively in superclasses
        field_values.append(&mut self.find_fields_for_superclasses(class));
        field_values
    }

    fn find_fields_for_class(&self, class: &Class) -> Vec<InstanceField> {
        class
            .fields
            .iter()
            .filter(|f| !f.access_flags.contains(&AccessFlag::Static))
            .map(|f| InstanceField {
                name: f.name.clone(),
                value: f.field_type.get_default_typed_value(),
            })
            .collect()
    }

    fn find_fields_for_interfaces(&self, class: &Class) -> Vec<InstanceField> {
        class
            .interface_names
            .iter()
            .map(
                |interface_name| match self.heap.get_class_by_name(interface_name) {
                    Some(result) => result,
                    None => panic!(
                        "could not load interface class {} of class {}",
                        interface_name, &class.name
                    ),
                },
            )
            .map(|(_index, interface)| interface)
            .flat_map(|interface| {
                let mut interface_fields = self.find_fields_for_class(interface);
                let mut recursive_fields = self.find_fields_for_interfaces(interface);
                interface_fields.append(&mut recursive_fields);
                interface_fields
            })
            .collect()
    }

    fn find_fields_for_superclasses(&self, class: &Class) -> Vec<InstanceField> {
        let (_index, superclass) = match self.heap.get_class_by_name(&class.super_class_name) {
            Some(result) => result,
            None => panic!(
                "could not load superclass class {} of class {}",
                &class.super_class_name, &class.name
            ),
        };
        if class.name.eq(&superclass.name) {
            return Vec::new();
        }
        let mut superclass_fields = self.find_fields_for_class(superclass);
        let mut recursive_fields = self.find_fields_for_superclasses(superclass);
        superclass_fields.append(&mut recursive_fields);
        superclass_fields
    }
}
