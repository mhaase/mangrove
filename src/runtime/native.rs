use crate::class_loader::class::{ReferenceTypedValue, TypedValue};

use super::data_areas::Frame;

pub struct Native {
    methods: Vec<NativeMethod>,
}

struct NativeMethod {
    class_name: String,
    method_name: String,
    implementation: fn(&Frame) -> TypedValue,
}

impl Native {
    pub fn bootstrap() -> Native {
        println!("loading native implementations");

        let mut instance = Native {
            methods: Vec::new(),
        };

        instance.add_native_method_impl("java/lang/Object", "hashCode", |frame| {
            println!("native java/lang/Object::hashCode");

            let reference = match &frame.local_variables[0] {
                TypedValue::Reference(value) => value,
                _ => panic!("expected Reference to instance in local variables stack"),
            };
            let instance_index = match reference {
                ReferenceTypedValue::Object(_class_name, _class_index, instance_index) => {
                    *instance_index
                }
                _ => panic!(
                    "expected Reference to instance of Object, but got: {:?}",
                    reference
                ),
            };
            TypedValue::Int(instance_index as i32)
        });

        instance.add_native_method_impl("java/lang/Class", "registerNatives", |_frame| {
            // nothing yet
            TypedValue::Void(())
        });
        instance.add_native_method_impl("java/lang/Class", "desiredAssertionStatus0", |_frame| {
            // we want assertions!
            TypedValue::Boolean(true)
        });
        instance.add_native_method_impl("java/lang/StringUTF16", "isBigEndian", |_frame| {
            // probably we are big endian
            TypedValue::Boolean(true)
        });
        instance.add_native_method_impl("java/lang/ClassLoader", "registerNatives", |_frame| {
            // do nothing
            TypedValue::Void(())
        });
        instance.add_native_method_impl("java/lang/System", "registerNatives", |_frame| {
            // nothing yet
            TypedValue::Void(())
        });

        instance
    }

    fn add_native_method_impl(
        &mut self,
        class_name: &str,
        method_name: &str,
        implementation: fn(&Frame) -> TypedValue,
    ) {
        self.methods.push(NativeMethod {
            class_name: class_name.to_owned(),
            method_name: method_name.to_owned(),
            implementation,
        });
    }

    pub fn has_native_method_implementation(&self, class_name: &str, method_name: &str) -> bool {
        self.methods
            .iter()
            .find(|m| m.class_name.eq(class_name) && m.method_name.eq(method_name))
            .is_some()
    }

    pub fn invoke(&self, frame: Frame, class_name: &str, method_name: &str) -> TypedValue {
        let native_method = match self
            .methods
            .iter()
            .find(|m| m.class_name.eq(class_name) && m.method_name.eq(method_name))
        {
            Some(native_method) => native_method,
            None => panic!(
                "could not find native method implementation for class: {} and method: {}",
                class_name, method_name
            ),
        };

        (native_method.implementation)(&frame)
    }
}
