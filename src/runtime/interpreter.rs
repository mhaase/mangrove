use crate::class_loader::{
    self,
    class::{
        AccessFlag, Class, ConstantPoolEntry, Field, Method, ReferenceType, ReferenceTypedValue,
        Type, TypedValue,
    },
};

use super::{
    data_areas::{Frame, Instance},
    Runtime,
};

pub fn interpret(
    runtime: &mut Runtime,
    class_index: usize,
    method_name: &str,
    parameters: Vec<TypedValue>,
    instance_index: Option<usize>,
) -> TypedValue {
    //TODO: method lookup in this class and superclasses. Very similar to invokespecial...
    // Maybe the method lookup only belongs into the invoke opcodes?
    let mut frame = {
        let class = runtime.heap.get_class_by_index(class_index);
        let method = class
            .methods
            .iter()
            .filter(|m| m.name.eq(method_name))
            .find(|m| {
                println!(
                    "checking method: {} with parameters: {:?}",
                    m.name, m.parameter_types
                );
                let matching_method_signature = m.parameter_types.len() == parameters.len()
                    && (0..parameters.len()).all(|i| {
                        println!(
                            "comparing parameter types: {} and {}",
                            parameters[i].get_type(),
                            &m.parameter_types[i]
                        );
                        // check if types of parameters match
                        // edge cases:
                        //   - some types are "masked" as Int (such as Short, unsure about the others)
                        //   - in String.class we have a case, where null is passed, and Void is expected -> should also match
                        //   - subtypes of expected parameter type can be supplied (only References)
                        parameters[i].get_type().eq(&m.parameter_types[i])
                            || (parameters[i].get_type().eq(&Type::Int) // "masked" Int
                                && match &m.parameter_types[i] {
                                    Type::Short | Type::Byte | Type::Boolean | Type::Char => true,
                                    _ => false,
                                })
                            || (parameters[i] // null passed to Reference parameter
                                .get_type()
                                .eq(&Type::Reference(ReferenceType::Null(None)))
                                && match m.parameter_types[i] {
                                    Type::Reference(_) => true,
                                    _ => false,
                                })
                            || is_supertype_of(
                                &runtime,
                                &m.parameter_types[i],
                                &parameters[i].get_type(),
                            )
                    });
                matching_method_signature
                // OR main without parameters
                    || (method_name.eq("main")
                        && m.parameter_types.len() == 1
                        && parameters.len() == 0)
            })
            .expect(&format!(
                "method not found: {} with parameters: {:?}",
                method_name, &parameters
            ));

        println!("# interpret: {}::{}", class.name, method_name);

        let instance = match instance_index {
            Some(instance_index) => match runtime.heap.get_instance_by_index(instance_index) {
                Some(instance) => Option::Some(instance),
                None => panic!(
                    "trying to invoke method {} on unknown instance_index {}",
                    method_name, instance_index
                ),
            },
            None => None,
        };
        create_frame(&class.name, class_index, instance, method, parameters)
    };

    if frame.native {
        return interpret_native(runtime, frame, method_name);
    }

    let mut current = 0;
    loop {
        let opcode = frame.opcodes[current];
        print!("! opcode {} ", opcode);
        match opcode {
            1 => {
                // aconst_null
                // push null
                println!("aconst_null");
                frame
                    .operand_stack
                    .push(TypedValue::Reference(ReferenceTypedValue::Null(
                        ReferenceType::Null(None),
                    )));
            }
            2 => {
                // iconst_m1
                println!("iconst_m1");
                frame.operand_stack.push(TypedValue::Int(-1));
            }
            3 => {
                // iconst_0
                println!("iconst_0");
                frame.operand_stack.push(TypedValue::Int(0));
            }
            4 => {
                // iconst_1
                println!("iconst_1");
                frame.operand_stack.push(TypedValue::Int(1));
            }
            5 => {
                // iconst_2
                println!("iconst_2");
                frame.operand_stack.push(TypedValue::Int(2));
            }
            6 => {
                // iconst_3
                println!("iconst_3");
                frame.operand_stack.push(TypedValue::Int(3));
            }
            7 => {
                // iconst_4
                println!("iconst_4");
                frame.operand_stack.push(TypedValue::Int(4));
            }
            8 => {
                // iconst_5
                println!("iconst_5");
                frame.operand_stack.push(TypedValue::Int(5));
            }
            16 => {
                // bipush
                print!("bipush");
                current += 1;
                let value = frame.opcodes[current] as i32;
                println!(" {}", value);

                frame.operand_stack.push(TypedValue::Int(value));
            }
            17 => {
                // sipush
                print!("bipush");
                current += 1;

                let bytes = frame.opcodes[current..current + 2].try_into().unwrap();
                let value = i16::from_be_bytes(bytes) as i32;
                current += 1;

                println!(" {}", value);

                frame.operand_stack.push(TypedValue::Int(value));
            }
            18 => {
                // ldc
                // load non-(long or double) from constant_pool, push onto stack
                print!("ldc");
                current += 1;

                let index = frame.opcodes[current] as usize;

                println!(" {}", index);
                let cp_entry = {
                    let class = runtime.heap.get_class_by_index(frame.class_index);
                    &class.constant_pool[index].clone()
                };
                match cp_entry {
                    ConstantPoolEntry::Int { value } => {
                        println!("pushed {} onto operand stack", value);
                        frame.operand_stack.push(value.clone());
                    }
                    ConstantPoolEntry::Float { value } => {
                        println!("pushed {} onto operand stack", value);
                        frame.operand_stack.push(value.clone());
                    }
                    ConstantPoolEntry::String { value } => {
                        frame.operand_stack.push(runtime.create_string_instance(value));
                    }
                    ConstantPoolEntry::Class { class_name } => {
                        //TODO: ahaaa, really pushing the Java class java/lang/Class with the Type as generic type
                        println!("pushing instance of: Class<{}> onto stack", class_name);
                        let generic_class_index = match runtime.heap.get_class_index_by_name(class_name) {
                            Some(i) => i,
                            None => panic!("could not find class: {}", class_name)
                        };
                        let (class_class_index,class_class_instance_index) = runtime.create_jvm_class_instance(generic_class_index);
                        frame.operand_stack.push(TypedValue::Reference(ReferenceTypedValue::Object("java/lang/Class".to_owned(), class_class_index, class_class_instance_index)));
                    }
                    _ => panic!(
                        "unexpected parameter for 'ldc': {} (maybe missing implementation, check docs)",
                        cp_entry
                    ),
                }
            }
            20 => {
                // ldc2_w
                // load long or double from constant_pool, push onto stack
                print!("ldc2_w");
                current += 1;

                let bytes = frame.opcodes[current..current + 2].try_into().unwrap();
                let index = i16::from_be_bytes(bytes) as usize;
                current += 1;

                println!(" {}", index);
                let cp_entry = {
                    let class = runtime.heap.get_class_by_index(frame.class_index);
                    &class.constant_pool[index].clone()
                };
                match cp_entry {
                    ConstantPoolEntry::Long { value } => {
                        println!("pushed {} onto operand stack", value);
                        frame.operand_stack.push(value.clone());
                    }
                    ConstantPoolEntry::Double { value } => {
                        println!("pushed {} onto operand stack", value);
                        frame.operand_stack.push(value.clone());
                    }
                    _ => panic!(
                        "expected Long as parameter for 'ldc2_w', but got: {} (maybe missing implementation, check docs)",
                        cp_entry
                    ),
                }
            }
            21 => {
                // iload
                print!("iload");
                current += 1;
                let index = frame.opcodes[current] as usize;
                println!(" {}", index);

                iload(index, &mut frame);
            }
            22 => {
                // lload
                print!("lload");
                current += 1;
                let index = frame.opcodes[current] as usize;
                println!(" {}", index);

                lload(index, &mut frame);
            }
            25 => {
                // aload
                print!("aload");
                current += 1;
                let index = frame.opcodes[current] as usize;
                println!(" {}", index);

                aload(index, &mut frame);
            }
            26 => {
                // iload_0
                println!("iload_0");
                iload(0, &mut frame);
            }
            27 => {
                // iload_1
                println!("iload_1");
                iload(1, &mut frame);
            }
            28 => {
                // iload_2
                println!("iload_2");
                iload(2, &mut frame);
            }
            29 => {
                // iload_3
                println!("iload_3");
                iload(3, &mut frame);
            }
            30 => {
                // lload_0
                println!("lload_0");
                lload(0, &mut frame);
            }
            31 => {
                // lload_1
                println!("lload_1");
                lload(1, &mut frame);
            }
            32 => {
                // lload_2
                println!("lload_2");
                lload(2, &mut frame);
            }
            33 => {
                // lload_3
                println!("lload_3");
                lload(3, &mut frame);
            }
            42 => {
                // aload_0
                println!("aload_0");
                aload(0, &mut frame);
            }
            43 => {
                // aload_1
                println!("aload_1");
                aload(1, &mut frame);
            }
            44 => {
                // aload_2
                println!("aload_2");
                aload(2, &mut frame);
            }
            45 => {
                // aload_3
                println!("aload_3");
                aload(3, &mut frame);
            }
            50 => {
                // aaload
                // load reference from array
                print!("aaload");

                let index = pop_int(&mut frame.operand_stack) as usize;
                let reference = pop_reference(&mut frame.operand_stack);
                print!(" {} {:?}", index, reference);
                match reference {
                    ReferenceTypedValue::Array(array_type, array_instance_index) => {
                        let array_instance = match runtime.heap.get_array_instance_by_index(array_instance_index) {
                            Some(arr) => arr,
                            None =>panic!("could not find array instance of type {} with array_instance_index: {}", array_type, array_instance_index),
                        };
                        let value = match array_instance.components.get(index) {
                            Some(v) => v,
                            None => {
                                panic!("//TODO: should throw an ArrayIndexOutOfBoundsException")
                            }
                        };
                        let value_to_push = match value {
                            TypedValue::Reference(..) => value.clone(),
                            _ => panic!(
                                "unexpected TypedValue found for aaload: {} (expected Reference)",
                                value
                            ),
                        };
                        println!(" -> {}", value_to_push);
                        frame.operand_stack.push(value_to_push);
                    }
                    ReferenceTypedValue::Null(_t) => {
                        panic!("//TODO: should throw a NullPointerException")
                    }
                    _ => panic!(
                        "expected array reference for aaload instruction, but got: {:?}",
                        reference
                    ),
                }
            }
            51 => {
                // baload
                // load byte or boolean from array
                print!("baload");

                let index = pop_int(&mut frame.operand_stack) as usize;
                let reference = pop_reference(&mut frame.operand_stack);
                print!(" {} {:?}", index, reference);
                match reference {
                    ReferenceTypedValue::Array(array_type, array_instance_index) => {
                        let array_instance = match runtime.heap.get_array_instance_by_index(array_instance_index) {
                            Some(arr) => arr,
                            None =>panic!("could not find array instance of type {} with array_instance_index: {}", array_type, array_instance_index),
                        };
                        let value = match array_instance.components.get(index) {
                            Some(v) => v,
                            None => {
                                panic!("//TODO: should throw an ArrayIndexOutOfBoundsException")
                            }
                        };
                        let value_as_int = match value {
                            TypedValue::Boolean(b) => TypedValue::Int(if *b {1} else {0}),
                            TypedValue::Byte(b) => TypedValue::Int(*b as i32),
                            _ => panic!(
                                "unexpected TypedValue found for baload: {} (expected Boolean or Byte)",
                                value
                            ),
                        };
                        println!(" -> {}", value_as_int);
                        frame.operand_stack.push(value_as_int);
                    }
                    _ => panic!(
                        "expected array reference for baload instruction, but got: {:?}",
                        reference
                    ),
                }
            }
            52 => {
                // caload
                // load char from array
                print!("caload");

                let index = pop_int(&mut frame.operand_stack) as usize;
                let reference = pop_reference(&mut frame.operand_stack);
                print!(" {} {:?}", index, reference);
                match reference {
                    ReferenceTypedValue::Array(array_type, array_instance_index) => {
                        if !array_type.eq(&Type::Char) {
                            panic!("expected array of type 'char', but got: {}", array_type);
                        }
                        let array_instance = match runtime.heap.get_array_instance_by_index(array_instance_index) {
                            Some(arr) => arr,
                            None =>panic!("could not find array instance of type {} with array_instance_index: {}", array_type, array_instance_index),
                        };
                        let value = match array_instance.components.get(index) {
                            Some(v) => v,
                            None => {
                                panic!("//TODO: should throw an ArrayIndexOutOfBoundsException")
                            }
                        };
                        let value_as_int = match value {
                            TypedValue::Char(c) => TypedValue::Int(*c as i32),
                            _ => panic!(
                                "unexpected TypedValue found for caload: {} (expected Char)",
                                value
                            ),
                        };
                        println!(" -> {}", value_as_int);
                        frame.operand_stack.push(value_as_int);
                    }
                    _ => panic!(
                        "expected array reference for caload instruction, but got: {:?}",
                        reference
                    ),
                }
            }
            54 => {
                // istore
                print!("istore ");
                current += 1;
                let index = frame.opcodes[current] as usize;
                print!(" {}", index);
                istore(index, &mut frame);
            }
            55 => {
                // lstore
                print!("lstore ");
                current += 1;
                let index = frame.opcodes[current] as usize;
                println!(" {}", index);
                lstore(index, &mut frame);
            }
            58 => {
                // astore
                print!("astore");
                current += 1;
                let index = frame.opcodes[current] as usize;
                print!(" {}", index);
                astore(index, &mut frame);
            }
            59 => {
                // istore_0
                print!("istore_0");
                istore(0, &mut frame);
            }
            60 => {
                // istore_1
                print!("istore_1");
                istore(1, &mut frame);
            }
            61 => {
                // istore_2
                print!("istore_2");
                istore(2, &mut frame);
            }
            62 => {
                // istore_3
                print!("istore_3");
                istore(3, &mut frame);
            }
            75 => {
                // astore_0
                print!("astore_0");

                astore(0, &mut frame);
            }
            76 => {
                // astore_1
                print!("astore_1");

                astore(1, &mut frame);
            }
            77 => {
                // astore_2
                print!("astore_2");

                astore(2, &mut frame);
            }
            78 => {
                // astore_3
                print!("astore_3");

                astore(3, &mut frame);
            }
            84 => {
                // bastore
                // store into byte or boolean array
                print!("bastore");
                let value = pop_int(&mut frame.operand_stack);
                let index = pop_int(&mut frame.operand_stack) as usize;
                print!(" {} {}", value, index);
                let reference = pop_reference(&mut frame.operand_stack);
                match reference {
                    ReferenceTypedValue::Array(array_type, array_instance_index) => {
                        let array_instance = match runtime.heap.get_array_instance_by_index_mut(array_instance_index) {
                            Some(arr) => arr,
                            None =>panic!("could not find array instance of type {} with array_instance_index: {}", array_type, array_instance_index),
                        };
                        if index >= array_instance.components.len() {
                            panic!("//TODO: should throw ArrayIndexOutOfBoundsException")
                        }
                        let result = match array_type {
                            Type::Boolean => TypedValue::Boolean(value != 0),
                            Type::Byte => TypedValue::Byte(value as i8),
                            _ => panic!("expected Type Byte or Boolean, but got: {}", array_type),
                        };
                        println!(" -> {}", result);
                        array_instance.components[index] = result;
                    }
                    _ => panic!(
                        "exptected array reference on stack, but got: {:?}",
                        reference
                    ),
                }
            }
            87 => {
                // pop
                println!("pop");
                let typed_value = frame.operand_stack.pop().expect("pop on empty stack");
                println!(" - popped: {}", &typed_value);
            }
            89 => {
                // dup
                print!("dup");
                let typed_value = frame.operand_stack.pop().expect("pop on empty stack");
                match typed_value {
                    TypedValue::Int(..)
                    | TypedValue::Byte(..)
                    | TypedValue::Boolean(..)
                    | TypedValue::Char(..)
                    | TypedValue::Short(..)
                    | TypedValue::Float(..)
                    | TypedValue::Reference(..) => {
                        println!(" {}", &typed_value);
                        frame.operand_stack.push(typed_value.clone());
                        frame.operand_stack.push(typed_value);
                    }
                    _ => panic!(
                        "expected value of a category 1 computational type on stack, but found: {}",
                        typed_value
                    ),
                }
            }
            96 => {
                // iadd
                println!("operand_stack: {:?}", frame.operand_stack);
                print!("iadd");
                let value1 = pop_int(&mut frame.operand_stack);
                let value2 = pop_int(&mut frame.operand_stack);
                println!(" {} {}", value1, value2);

                let result = value1 + value2;
                frame.operand_stack.push(TypedValue::Int(result));
                println!("operand_stack: {:?}", frame.operand_stack);
            }
            100 => {
                // isub
                println!("operand_stack: {:?}", frame.operand_stack);
                print!("isub");
                let value1 = pop_int(&mut frame.operand_stack);
                let value2 = pop_int(&mut frame.operand_stack);
                println!(" {} {}", value1, value2);

                let result = value1 - value2;
                frame.operand_stack.push(TypedValue::Int(result));
                println!("operand_stack: {:?}", frame.operand_stack);
            }
            104 => {
                // imul
                print!("imul");
                let value2 = pop_int(&mut frame.operand_stack);
                let value1 = pop_int(&mut frame.operand_stack);
                println!(" {} {}", value1, value2);

                let result = value1 * value2;
                frame.operand_stack.push(TypedValue::Int(result));
            }
            116 => {
                // ineg
                print!("ineg");
                let value = pop_int(&mut frame.operand_stack);
                println!(" {}", value);

                let result = -value;
                frame.operand_stack.push(TypedValue::Int(result));
            }
            120 => {
                // ishl
                print!("ishl");
                let value2 = pop_int(&mut frame.operand_stack);
                let value1 = pop_int(&mut frame.operand_stack);
                print!(" {} {}", value1, value2);

                let result = value1 << value2;
                println!(" -> {}", result);
                frame.operand_stack.push(TypedValue::Int(result));
            }
            122 => {
                // ishr
                print!("ishr");
                let value2 = pop_int(&mut frame.operand_stack);
                let value1 = pop_int(&mut frame.operand_stack);
                print!(" {} {}", value1, value2);

                let result = value1 >> value2;
                println!(" -> {}", result);
                frame.operand_stack.push(TypedValue::Int(result));
            }
            132 => {
                // iinc
                print!("iinc");

                current += 1;
                let index = frame.opcodes[current] as usize;
                current += 1;
                let constant = (frame.opcodes[current] as i8) as i32;
                print!("  {} {}", index, constant);

                let value = match frame.local_variables.get(index) {
                    Some(value) => match value {
                        TypedValue::Int(v) => v,
                        _ => panic!("expected Int value, but got: {}", value),
                    },
                    None => panic!(
                        "index does not exist in local variables (len={}): {}",
                        frame.local_variables.len(),
                        index
                    ),
                };
                print!(" ({})", value);

                let result = value + constant;
                println!(" -> {}", result);
                frame.local_variables[index] = TypedValue::Int(result);
            }
            136 => {
                // l2i
                print!("l2i");
                let value = pop_long(&mut frame.operand_stack);
                print!(" {}", value);

                let result = value as i32;
                println!(" -> {}", result);
                frame.operand_stack.push(TypedValue::Int(result));
            }
            145 => {
                // ib2
                print!("i2b");
                let value = pop_int(&mut frame.operand_stack);
                print!(" {}", value);

                let result = (value as i8) as i32;
                println!(" -> {}", result);
                frame.operand_stack.push(TypedValue::Int(result));
            }
            153 => {
                // ifeq
                print!("ifeq ");
                let comparison = |value| value == 0;

                let new_current = if_cond(comparison, current, &mut frame);
                current = new_current;
                continue;
            }
            154 => {
                // ifne
                print!("ifne ");
                let comparison = |value| value != 0;

                let new_current = if_cond(comparison, current, &mut frame);
                current = new_current;
                continue;
            }
            155 => {
                // iflt
                print!("iflt ");
                let comparison = |value| value < 0;

                let new_current = if_cond(comparison, current, &mut frame);
                current = new_current;
                continue;
            }
            156 => {
                // ifge
                print!("ifge ");
                let comparison = |value| value >= 0;

                let new_current = if_cond(comparison, current, &mut frame);
                current = new_current;
                continue;
            }
            157 => {
                // ifgt
                print!("ifgt ");
                let comparison = |value| value > 0;

                let new_current = if_cond(comparison, current, &mut frame);
                current = new_current;
                continue;
            }
            158 => {
                // ifle
                print!("ifle ");
                let comparison = |value| value <= 0;

                let new_current = if_cond(comparison, current, &mut frame);
                current = new_current;
                continue;
            }
            159 => {
                // if_icmpeq
                print!("if_icmpeq ");
                let comparison = |a, b| a == b;

                let new_current = if_icmp(comparison, current, &mut frame);
                current = new_current;
                continue;
            }
            160 => {
                // if_icmpne
                print!("if_icmpne ");
                let comparison = |a, b| a != b;

                let new_current = if_icmp(comparison, current, &mut frame);
                current = new_current;
                continue;
            }
            161 => {
                // if_icmplt
                print!("if_icmplt ");
                let comparison = |a, b| a < b;

                let new_current = if_icmp(comparison, current, &mut frame);
                current = new_current;
                continue;
            }
            162 => {
                // if_icmpge
                print!("if_icmpge ");
                let comparison = |a, b| a >= b;

                let new_current = if_icmp(comparison, current, &mut frame);
                current = new_current;
                continue;
            }
            163 => {
                // if_icmpgt
                print!("if_icmpgt ");
                let comparison = |a, b| a > b;

                let new_current = if_icmp(comparison, current, &mut frame);
                current = new_current;
                continue;
            }
            164 => {
                // if_icmple
                print!("if_icmple ");
                let comparison = |a, b| a <= b;

                let new_current = if_icmp(comparison, current, &mut frame);
                current = new_current;
                continue;
            }
            167 => {
                // goto
                // finally, I get to use goto again :)
                print!("goto");
                current += 1;
                let branchbyte1 = frame.opcodes[current];
                current += 1;
                let branchbyte2 = frame.opcodes[current];

                // minus 2, because we incremented twice before to get the branchbytes (but the base for the jump is the instruction itself)
                current =
                    calculate_new_opcode_index_by_offset(branchbyte1, branchbyte2, current) - 2;
                continue;
            }
            172 => {
                // ireturn
                print!("ireturn");
                let value = pop_int(&mut frame.operand_stack);
                let intermediate_result = match frame.return_type.get_specific_int_type_value(value)
                {
                    Some(v) => v,
                    None => panic!(
                        "wrong return type of method for ireturn operation: {}",
                        frame.return_type
                    ),
                };
                print!(" {}", &intermediate_result);
                let result = intermediate_result.convert_to_int();
                println!(" -> {}", &result);
                log_end_interpret(runtime, &frame, method_name);
                return result;
            }
            176 => {
                // areturn
                print!("areturn");
                let reference = pop_reference(&mut frame.operand_stack);
                let result = TypedValue::Reference(reference);
                println!(" {}", &result);
                log_end_interpret(runtime, &frame, method_name);
                return result;
            }
            177 => {
                // return
                println!("return");
                log_end_interpret(runtime, &frame, method_name);
                return TypedValue::Void(());
            }
            178 => {
                // getstatic
                print!("getstatic");
                current += 1;

                let bytes = frame.opcodes[current..current + 2].try_into().unwrap();
                let index = i16::from_be_bytes(bytes) as usize;
                current += 1;

                println!(" {}", index);
                let cp_entry = {
                    let class = runtime.heap.get_class_by_index(frame.class_index);
                    &class.constant_pool[index].clone()
                };
                match cp_entry {
                    ConstantPoolEntry::Fieldref {
                        class_name,
                        field_name,
                        field_type,
                    } => {
                        println!("cp_entry: {}", cp_entry);
                        let field =
                            static_field_lookup(runtime, class_name, field_name, field_type);

                        let value = field.value.clone();
                        println!("pushing {} onto operand stack", &value);
                        frame.operand_stack.push(value);
                    }
                    _ => panic!(
                        "expected Fieldref as parameter for 'getstatic', but got: {}",
                        cp_entry
                    ),
                }
            }
            179 => {
                // putstatic
                print!("putstatic");
                current += 1;
                let index = class_loader::bytes_to_u16(&frame.opcodes, current) as usize;
                current += 1;
                println!(" {}", index);
                let cp_entry = {
                    let class = runtime.heap.get_class_by_index(frame.class_index);
                    &class.constant_pool[index].clone()
                };
                match cp_entry {
                    ConstantPoolEntry::Fieldref {
                        class_name,
                        field_name,
                        field_type,
                    } => {
                        println!("cp_entry: {}", cp_entry);
                        let mut field =
                            static_field_lookup_mut(runtime, class_name, field_name, field_type);

                        let value = frame.operand_stack.pop().expect("pop on empty stack");
                        println!("set static value: {}", &value);
                        field.value = value;
                    }
                    _ => panic!(
                        "expected Fieldref as parameter for 'putstatic', but got: {}",
                        cp_entry
                    ),
                }
            }
            180 => {
                // getfield
                print!("getfield");
                current += 1;
                let index = class_loader::bytes_to_u16(&frame.opcodes, current) as usize;
                current += 1;
                println!(" {}", index);
                let cp_entry = {
                    let class = runtime.heap.get_class_by_index(frame.class_index);
                    &class.constant_pool[index].clone()
                };
                match cp_entry {
                    ConstantPoolEntry::Fieldref {
                        class_name: _,
                        field_name,
                        field_type: _,
                    } => {
                        println!("cp_entry: {}", cp_entry);

                        let reference = pop_reference(&mut frame.operand_stack);

                        match reference {
                            ReferenceTypedValue::Object(
                                class_name,
                                _class_index,
                                instance_index,
                            ) => {
                                let instance =
                                    match runtime.heap.get_instance_by_index(instance_index) {
                                        Some(x) => x,
                                        None => panic!(
                                            "could not find instance of {} with index: {}",
                                            &class_name, instance_index
                                        ),
                                    };
                                let field = instance
                                    .field_values
                                    .iter()
                                    .find(|f| f.name.eq(field_name))
                                    .expect("field not found");

                                let value = field.value.clone();
                                frame.operand_stack.push(value);
                            }
                            _ => panic!("array or unknown reference not allowed"),
                        }
                    }
                    _ => panic!(
                        "expected Fieldref as parameter for 'putfield', but got: {}",
                        cp_entry
                    ),
                }
            }
            181 => {
                // putfield
                print!("putfield");
                current += 1;
                let index = class_loader::bytes_to_u16(&frame.opcodes, current) as usize;
                current += 1;
                println!(" {}", index);
                let cp_entry = {
                    let class = runtime.heap.get_class_by_index(frame.class_index);
                    &class.constant_pool[index].clone()
                };
                match cp_entry {
                    ConstantPoolEntry::Fieldref {
                        class_name: _,
                        field_name,
                        field_type,
                    } => {
                        println!("cp_entry: {}", cp_entry);

                        println!("operand stack: {:?}", &frame.operand_stack);

                        let value = frame.operand_stack.pop().expect("pop on empty stack");
                        let reference = pop_reference(&mut frame.operand_stack);

                        match reference {
                            ReferenceTypedValue::Object(
                                class_name,
                                _class_index,
                                instance_index,
                            ) => {
                                let value_type = value.get_type();
                                let value_and_field_types_match = field_type.eq(&value_type)
                                    || match field_type {
                                        // these types are converted to Int often
                                        Type::Boolean | Type::Byte | Type::Char | Type::Short => {
                                            true
                                        }
                                        // null References are allowed for References
                                        Type::Reference(_) => value_type
                                            .eq(&Type::Reference(ReferenceType::Null(None))),
                                        _ => false,
                                    }
                                    || is_supertype_of(runtime, field_type, &value_type);
                                if !value_and_field_types_match {
                                    panic!(
                                        "new value type ({}) does not match field type ({})",
                                        value.get_type(),
                                        field_type
                                    );
                                }

                                let instance =
                                    match runtime.heap.get_instance_by_index_mut(instance_index) {
                                        Some(x) => x,
                                        None => panic!(
                                            "could not find instance of {} with index: {}",
                                            &class_name, instance_index
                                        ),
                                    };
                                let field = instance
                                    .field_values
                                    .iter_mut()
                                    .find(|f| f.name.eq(field_name))
                                    .expect("field not found");

                                println!("setting new field value: {}", &value);
                                field.value = value;
                            }
                            _ => panic!("array or unknown reference not allowed"),
                        }
                    }
                    _ => panic!(
                        "expected Fieldref as parameter for 'putfield', but got: {}",
                        cp_entry
                    ),
                }
            }
            182 => {
                // invokevirtual
                print!("invokevirtual");
                current += 1;
                let index = class_loader::bytes_to_u16(&frame.opcodes, current) as usize;
                current += 1;
                println!(" {}", index);
                let cp_entry = {
                    let class = runtime.heap.get_class_by_index(frame.class_index);
                    &class.constant_pool[index].clone()
                };
                match cp_entry {
                    ConstantPoolEntry::Methodref {
                        class_name,
                        method_name,
                        return_type,
                        parameter_types,
                    } => {
                        println!("invokevirtual: {}", cp_entry);

                        let (invoke_class_index, invoke_class) =
                            match runtime.heap.get_class_by_name(&class_name) {
                                Some((i, c)) => (i, c),
                                None => panic!("could not find class: {}", class_name),
                            };
                        //TODO: another method lookup spot
                        let invoke_method =
                            match invoke_class.methods.iter().find(|m| m.name.eq(method_name)) {
                                Some(m) => m,
                                None => panic!(
                                    "method not found: {} in class: {}",
                                    method_name, class_name
                                ),
                            };

                        if invoke_method.access_flags.contains(&AccessFlag::Native)
                            && invoke_method.access_flags.contains(&AccessFlag::Native)
                        {
                            //TODO: implement flow for signature-polymorphic methods
                            panic!("flow for signature-polymorphic methods not yet implemented");
                        }

                        // pop as many values from operand stack, as there are parameters in the method descriptor
                        let invoke_parameters = pop_invoke_parameters(parameter_types, &mut frame);
                        let invoke_instance_index = pop_invoke_reference(&mut frame);

                        let result = interpret(
                            runtime,
                            invoke_class_index,
                            &method_name,
                            invoke_parameters,
                            Some(invoke_instance_index),
                        );
                        process_invocation_result(runtime, result, &mut frame, return_type);
                    }
                    _ => panic!(
                        "expected Methodref as parameter for 'invokevirtual', but got: {}",
                        cp_entry
                    ),
                }
            }
            183 => {
                // invokespecial
                print!("invokespecial");
                current += 1;
                let index = class_loader::bytes_to_u16(&frame.opcodes, current) as usize;
                current += 1;
                println!(" {}", index);
                let cp_entry = {
                    let class = runtime.heap.get_class_by_index(frame.class_index);
                    &class.constant_pool[index].clone()
                };
                match cp_entry {
                    ConstantPoolEntry::Methodref {
                        class_name,
                        method_name,
                        return_type,
                        parameter_types,
                    } => {
                        println!("invokespecial: {}", cp_entry);

                        // pop as many values from operand stack, as there are parameters in the method descriptor
                        let invoke_parameters = pop_invoke_parameters(parameter_types, &mut frame);
                        let invoke_instance_index = pop_invoke_reference(&mut frame);

                        // lookup class
                        let invoke_class_index =
                            match runtime.heap.get_class_index_by_name(&class_name) {
                                Some(class_index) => class_index,
                                None => panic!("could not find class by name: {}", &class_name),
                            };

                        // TODO: select method to be invoked
                        // 1) lookup method (same name and descriptor) in class
                        // 2) lookup method in superclasses
                        // 3) go up until java/lang/Object to find method
                        // 4) more complex method lookup - find "maximally-specific method"

                        // invoke the method (same instance, but superclass)
                        let result = interpret(
                            runtime,
                            invoke_class_index,
                            &method_name,
                            invoke_parameters,
                            Some(invoke_instance_index),
                        );
                        process_invocation_result(runtime, result, &mut frame, return_type);
                    }
                    _ => panic!(
                        "expected Methodref as parameter for 'invokespecial', but got: {}",
                        cp_entry
                    ),
                }
            }
            184 => {
                // invokestatic
                print!("invokestatic");
                current += 1;
                let index = class_loader::bytes_to_u16(&frame.opcodes, current) as usize;
                current += 1;
                println!(" {}", index);
                let cp_entry = {
                    let class = runtime.heap.get_class_by_index(frame.class_index);
                    &class.constant_pool[index].clone()
                };
                match cp_entry {
                    ConstantPoolEntry::Methodref {
                        class_name,
                        method_name,
                        return_type,
                        parameter_types,
                    } => {
                        println!("invokestatic: {}", cp_entry);

                        let (invoke_class_index, invoke_class) =
                            match runtime.heap.get_class_by_name(&class_name) {
                                Some((i, c)) => (i, c),
                                None => panic!("could not find class: {}", class_name),
                            };
                        //TODO: another method lookup spot
                        let invoke_method =
                            match invoke_class.methods.iter().find(|m| m.name.eq(method_name)) {
                                Some(m) => m,
                                None => panic!(
                                    "method not found: {} in class: {}",
                                    method_name, class_name
                                ),
                            };

                        if invoke_method.access_flags.contains(&AccessFlag::Abstract)
                            || !invoke_method.access_flags.contains(&AccessFlag::Static)
                        {
                            panic!("invokestatic invoked for non-static or abstract method");
                        }

                        // pop as many values from operand stack, as there are parameters in the method descriptor
                        let invoke_parameters = pop_invoke_parameters(parameter_types, &mut frame);

                        let result = interpret(
                            runtime,
                            invoke_class_index,
                            &method_name,
                            invoke_parameters,
                            None,
                        );
                        process_invocation_result(runtime, result, &mut frame, return_type);
                    }
                    _ => panic!(
                        "expected Methodref as parameter for 'invokestatic', but got: {}",
                        cp_entry
                    ),
                }
            }
            185 => {
                // invokeinterface
                print!("invokeinterface");
                current += 1;
                let index = class_loader::bytes_to_u16(&frame.opcodes, current) as usize;
                current += 2;
                let count = frame.opcodes[current];
                current += 1;
                let zero_operand = frame.opcodes[current];
                println!(" {} {} {}", index, count, zero_operand);
                let cp_entry = {
                    let class = runtime.heap.get_class_by_index(frame.class_index);
                    &class.constant_pool[index].clone()
                };
                match cp_entry {
                    ConstantPoolEntry::InterfaceMethodref {
                        class_name:_,
                        method_name,
                        return_type,
                        parameter_types,
                    } => {
                        println!("invokeinterface: {}", cp_entry);

                        let invoke_parameters = pop_invoke_parameters(parameter_types, &mut frame);
                        let invoke_instance_index = pop_invoke_reference(&mut frame);
                        let instance=match runtime.heap.get_instance_by_index(invoke_instance_index) {
                            Some(i) =>i,
                            None => panic!("could not find instance with index: {}", invoke_instance_index),
                        };

                        //TODO: could verify if types are matching

                        let result = interpret(
                            runtime,
                            instance.class_index,
                            &method_name,
                            invoke_parameters,
                            None,
                        );
                        process_invocation_result(runtime, result, &mut frame, return_type);
                    }
                    _ => panic!(
                        "expected InterfaceMethodref as parameter for 'invokeinterface', but got: {}",
                        cp_entry
                    ),
                }
            }
            187 => {
                // new
                print!("new");
                current += 1;
                let index = class_loader::bytes_to_u16(&frame.opcodes, current) as usize;
                current += 1;
                println!(" {}", index);
                let cp_entry = {
                    let class = runtime.heap.get_class_by_index(frame.class_index);
                    &class.constant_pool[index].clone()
                };
                match cp_entry {
                    ConstantPoolEntry::Class { class_name } => {
                        // hmmm
                        let new_class_index =
                            match runtime.heap.get_class_index_by_name(&class_name) {
                                Some(result) => result,
                                None => {
                                    panic!("Could not find class for new operation: {}", class_name)
                                }
                            };
                        let instance_index = runtime.create_instance_uninitialised(new_class_index);
                        let class_name = class_name.to_owned();

                        frame.operand_stack.push(TypedValue::Reference(
                            ReferenceTypedValue::Object(
                                class_name,
                                new_class_index,
                                instance_index,
                            ),
                        ))
                    }
                    _ => panic!(
                        "expected Class as parameter for 'new', but got: {}",
                        cp_entry
                    ),
                }
            }
            188 => {
                // newarray
                // used for primitive types
                print!("newarray");
                let count = pop_int(&mut frame.operand_stack);
                print!(" {}", count);
                if count < 0 {
                    panic!("trying to create array with negative dimensions");
                }
                current += 1;
                let atype = &frame.opcodes[current];
                let array_type = match *atype {
                    4 => Type::Boolean,
                    5 => Type::Char,
                    6 => Type::Float,
                    7 => Type::Double,
                    8 => Type::Byte,
                    9 => Type::Short,
                    10 => Type::Int,
                    11 => Type::Long,
                    _ => panic!("\nunknown atype: {}", *atype),
                };
                println!(" {}", &array_type);

                let mut components = Vec::new();
                for _i in 0..count {
                    components.push(array_type.get_default_typed_value());
                }

                let array_instance_index =
                    runtime.create_array_instance(array_type.clone(), components);

                frame
                    .operand_stack
                    .push(TypedValue::Reference(ReferenceTypedValue::Array(
                        array_type,
                        array_instance_index,
                    )))
            }
            189 => {
                // anewarray
                // used for reference types
                print!("anewarray");
                current += 1;
                let index = class_loader::bytes_to_u16(&frame.opcodes, current) as usize;
                current += 1;
                print!(" {}, ", index);
                let cp_entry = {
                    let class = runtime.heap.get_class_by_index(frame.class_index);
                    &class.constant_pool[index].clone()
                };
                let count = pop_int(&mut frame.operand_stack);
                println!("count: {}", count);
                if count < 0 {
                    panic!("trying to create array with negative dimensions");
                }

                match cp_entry {
                    ConstantPoolEntry::Class { class_name } => {
                        let class_name = class_name.to_owned();
                        println!("  - reference: {}", &class_name);

                        let reference_type = ReferenceType::Object(class_name);
                        let mut components = Vec::new();
                        for _i in 0..count {
                            components.push(TypedValue::Reference(ReferenceTypedValue::Null(ReferenceType::Null(Some(Box::new(reference_type.clone()))))));
                        }

                        let object_type = Type::Reference(reference_type);
                        let array_instance_index = runtime.create_array_instance(object_type.clone(), components);

                        frame
                            .operand_stack
                            .push(TypedValue::Reference(ReferenceTypedValue::Array(
                                object_type,array_instance_index
                            )))
                    }
                    _ => panic!(
                        "expected Class/Interface/Array as parameter for 'anewarray', but got: {} (probably missing implementation)",
                        cp_entry
                    ),
                }
            }
            190 => {
                // arraylength
                print!("arraylength");
                let reference = pop_reference(&mut frame.operand_stack);
                match reference {
                    ReferenceTypedValue::Array(array_type, array_instance_index) => {
                        let array_instance = match runtime.heap.get_array_instance_by_index(array_instance_index) {
                            Some(arr) => arr,
                            None => panic!("could not find array instance of type {} with array_instance_index: {}", array_type, array_instance_index)
                        };
                        let arraylength = array_instance.components.len();
                        println!(" of type {}: {}", array_type, arraylength);
                        frame
                            .operand_stack
                            .push(TypedValue::Int(arraylength as i32))
                    }
                    _ => panic!(
                        "\nexpected array reference on operand stack, but got: {:?}",
                        reference
                    ),
                }
            }
            192 => {
                // checkcast
                print!("checkcast");
                current += 1;

                let bytes = frame.opcodes[current..current + 2].try_into().unwrap();
                let index = i16::from_be_bytes(bytes) as usize;
                current += 1;

                print!(" {}", index);
                let supertype_entry = {
                    let class = runtime.heap.get_class_by_index(frame.class_index);
                    &class.constant_pool[index].clone()
                };

                print!(" {}", supertype_entry);

                let possible_subtype_reference = pop_reference(&mut frame.operand_stack);
                println!(" {:?}", &possible_subtype_reference);

                match supertype_entry {
                    ConstantPoolEntry::Class { class_name: supertype_class_name } => {
                        match &possible_subtype_reference {
                            ReferenceTypedValue::Object(possible_subtype_class_name, _possible_subtype_class_index, _possible_subtype_instance_index)
                            | ReferenceTypedValue::Interface(possible_subtype_class_name, _possible_subtype_class_index, _possible_subtype_instance_index) => {
                                if !is_supertype_of(runtime, &Type::Reference(ReferenceType::Object(supertype_class_name.to_owned())), &Type::Reference(ReferenceType::Object(possible_subtype_class_name.to_owned()))) {
                                    panic!("//TODO: should throw ClassCastException")
                                }
                            },
                            ReferenceTypedValue::Array(array_type, array_instance_index) => {
                                panic!("castcheck array not implemented yet")
                            }
                            ReferenceTypedValue::Null(_t) => {
                                // "If objectref is null, then the operand stack is unchanged."
                            },
                            _ => panic!("unexpected reference type: {:?}", possible_subtype_reference),
                        }
                    },
                    _ => panic!("unexpected cp_entry for checkcast: {} (expected Class, maybe missing implementation)", supertype_entry),
                }

                frame
                    .operand_stack
                    .push(TypedValue::Reference(possible_subtype_reference));
            }
            194 => {
                // monitorenter
                println!("monitorenter");
                println!("  -> not implemented yet, will simply do nothing")
            }
            198 => {
                // ifnull
                print!("ifnull");
                let comparison = |value| match value {
                    ReferenceTypedValue::Null(_t) => true,
                    _ => false,
                };

                let new_current = if_reference(comparison, current, &mut frame);
                current = new_current;
                continue;
            }
            199 => {
                // ifnonnull
                print!("ifnonnull");
                let comparison = |value| match value {
                    ReferenceTypedValue::Null(_t) => false,
                    _ => true,
                };

                let new_current = if_reference(comparison, current, &mut frame);
                current = new_current;
                continue;
            }

            _ => panic!("unknown opcode: {}", opcode),
        }

        current += 1;
    }
}

fn is_supertype_of(runtime: &Runtime, supertype: &Type, possible_subtype: &Type) -> bool {
    println!(
        "?? checking if {} is supertype of {}",
        supertype, possible_subtype
    );
    // only works for References
    let supertype_name = match supertype {
        Type::Reference(reference_type) => match reference_type {
            ReferenceType::Object(n) => n,
            ReferenceType::Interface(n) => n,
            _ => return false,
        },
        _ => return false,
    };

    match possible_subtype {
        Type::Reference(reference_subtype) => match reference_subtype {
            ReferenceType::Object(subclass_name) => {
                let (_subclass_index, subclass) =
                    runtime.heap.get_class_by_name(&subclass_name).unwrap();
                // check if supertype is an interface of possible_subtype (recursively)
                for interface_name in &subclass.interface_names {
                    println!("checking interface_name: {}", interface_name);

                    if interface_name.eq(supertype_name) {
                        return true;
                    }
                    let interface_type =
                        &Type::Reference(ReferenceType::Interface(interface_name.to_owned()));
                    println!("checking recursive interface: {}", interface_type);
                    if is_supertype_of(runtime, supertype, interface_type) {
                        return true;
                    }
                }
                // if subclass.super_class_name.eq("java/lang/Object") {
                //     println!("stopping at: {}", subclass.super_class_name);
                //     return false;
                // }
                println!("checking superclass name: {}", subclass.super_class_name);
                // check if supertype is a superclass of possible_subtype (recursively)
                if subclass.super_class_name.eq(supertype_name) {
                    return true;
                }
                let superclass =
                    &Type::Reference(ReferenceType::Object(subclass.super_class_name.to_owned()));
                println!("checking recursive superclass: {}", superclass);
                is_supertype_of(runtime, supertype, superclass)
            }
            _ => false,
        },
        _ => false,
    }
}

fn if_icmp(
    comparison: fn(i32, i32) -> bool,
    current_opcode_index: usize,
    frame: &mut Frame,
) -> usize {
    let mut opcode_index = current_opcode_index;
    opcode_index += 1;
    let branchbyte1 = frame.opcodes[opcode_index];
    opcode_index += 1;
    let branchbyte2 = frame.opcodes[opcode_index];

    let value2 = pop_int(&mut frame.operand_stack);
    let value1 = pop_int(&mut frame.operand_stack);
    print!(" {} {} ({} {})", branchbyte1, branchbyte2, value1, value2);
    let result = (comparison)(value1, value2);
    println!(" -> {}", result);
    if result {
        // minus 2, because we incremented twice before to get the branchbytes (but the base for the jump is the instruction itself)
        return calculate_new_opcode_index_by_offset(branchbyte1, branchbyte2, opcode_index) - 2;
    }
    println!("continuing 'normally'");
    return opcode_index + 1;
}

fn if_cond(comparison: fn(i32) -> bool, current_opcode_index: usize, frame: &mut Frame) -> usize {
    let mut opcode_index = current_opcode_index;
    opcode_index += 1;
    let branchbyte1 = frame.opcodes[opcode_index];
    opcode_index += 1;
    let branchbyte2 = frame.opcodes[opcode_index];

    let value = pop_int(&mut frame.operand_stack);
    print!(" {} {} ({})", branchbyte1, branchbyte2, value);
    let result = (comparison)(value);
    println!(" -> {}", result);
    if result {
        // minus 2, because we incremented twice before to get the branchbytes (but the base for the jump is the instruction itself)
        return calculate_new_opcode_index_by_offset(branchbyte1, branchbyte2, opcode_index) - 2;
    }
    println!("continuing 'normally'");
    return opcode_index + 1;
}

fn if_reference(
    comparison: fn(ReferenceTypedValue) -> bool,
    current_opcode_index: usize,
    frame: &mut Frame,
) -> usize {
    let mut opcode_index = current_opcode_index;
    opcode_index += 1;
    let branchbyte1 = frame.opcodes[opcode_index];
    opcode_index += 1;
    let branchbyte2 = frame.opcodes[opcode_index];

    let value = pop_reference(&mut frame.operand_stack);
    print!(" {} {} ({:?})", branchbyte1, branchbyte2, value);
    let result = (comparison)(value);
    println!(" -> {}", result);
    if result {
        // minus 2, because we incremented twice before to get the branchbytes (but the base for the jump is the instruction itself)
        return calculate_new_opcode_index_by_offset(branchbyte1, branchbyte2, opcode_index) - 2;
    }
    println!("continuing 'normally'");
    return opcode_index + 1;
}

fn calculate_new_opcode_index_by_offset(
    branchbyte1: u8,
    branchbyte2: u8,
    opcode_index: usize,
) -> usize {
    let mut new_opcode_index = opcode_index;
    let offset = i16::from_be_bytes([branchbyte1, branchbyte2]);
    println!(" offset: {}", offset);
    if offset < 0 {
        new_opcode_index -= (-offset) as usize;
    } else {
        new_opcode_index += offset as usize;
    }
    new_opcode_index
}

fn interpret_native(runtime: &mut Runtime, frame: Frame, method_name: &str) -> TypedValue {
    let class = runtime.heap.get_class_by_index(frame.class_index);

    runtime.native.invoke(frame, &class.name, method_name)
}

fn process_invocation_result(
    runtime: &Runtime,
    result: TypedValue,
    frame: &mut Frame,
    expected_return_type: &Type,
) {
    // validate result type
    let result_type = result.get_type();
    if !result_type.eq(&expected_return_type) {
        let is_null_reference = match expected_return_type {
            Type::Reference(_t) => match &result_type {
                Type::Reference(rt) => match rt {
                    ReferenceType::Null(_t) => true,
                    _ => false,
                },
                _ => false,
            },
            _ => false,
        };
        let is_converted_int_value = result_type.eq(&Type::Int)
            && match expected_return_type {
                Type::Boolean | Type::Byte | Type::Char | Type::Short => true,
                _ => false,
            };
        let is_super_type = is_supertype_of(runtime, expected_return_type, &result_type);
        if !is_null_reference && !is_converted_int_value && !is_super_type {
            panic!(
                "invocation result has different type ({}) than expected ({})",
                result_type, expected_return_type
            );
        }
    }
    if result.get_type().eq(&Type::Void) {
        println!("> not pushing result <Void> onto operand stack")
    } else {
        // push result onto operand stack
        frame.operand_stack.push(result);
    }
    println!("operand stack after invocation: {:?}", frame.operand_stack);
}

fn pop_invoke_reference(frame: &mut Frame) -> usize {
    // pop reference to instance from operand stack
    let reference = pop_reference(&mut frame.operand_stack);
    println!("popped reference to: {:?}", &reference);
    let invoke_instance_index = match reference {
        ReferenceTypedValue::Object(_class_name, _class_index, instance_index) => instance_index,
        _ => panic!(
            "unsupported/unimplemented reference type for invocation: {:?}",
            reference
        ),
    };
    invoke_instance_index
}

fn pop_invoke_parameters(parameter_types: &Vec<Type>, frame: &mut Frame) -> Vec<TypedValue> {
    println!(
        "## pop_invoke_parameters, operand_stack: {:?}",
        frame.operand_stack
    );
    let mut invoke_parameters = Vec::new();
    // collect parameters in reverse
    for parameter_type in parameter_types.iter().rev() {
        let invoke_parameter = match parameter_type {
            //TODO: it feels weird that all "lesser" numbers are stored and handled as int
            Type::Boolean => TypedValue::Int(pop_int(&mut frame.operand_stack)),
            Type::Byte => TypedValue::Int(pop_int(&mut frame.operand_stack)),
            Type::Int => TypedValue::Int(pop_int(&mut frame.operand_stack)),
            Type::Short => TypedValue::Int(pop_int(&mut frame.operand_stack)),
            Type::Char => TypedValue::Int(pop_int(&mut frame.operand_stack)),
            Type::Long => TypedValue::Long(pop_long(&mut frame.operand_stack)),
            Type::Double => TypedValue::Double(pop_double(&mut frame.operand_stack)),
            Type::Reference(_t) => TypedValue::Reference(pop_reference(&mut frame.operand_stack)),
            //TODO: add more Types... maybe extract into method. Didn't I do that already somewhere? :)
            _ => panic!(
                "could not pop value of type {} from operand stack",
                parameter_type
            ),
        };
        println!("### popped: {}", &invoke_parameter);
        invoke_parameters.push(invoke_parameter);
    }
    // revert the reversion :)
    invoke_parameters.reverse();
    invoke_parameters
}

fn log_end_interpret(runtime: &mut Runtime, frame: &Frame, method_name: &str) {
    let class = runtime.heap.get_class_by_index(frame.class_index);
    println!("# end interpret: {}::{}", class.name, method_name);
}

fn create_frame(
    class_name: &str,
    class_index: usize,
    instance: Option<&Instance>,
    method: &Method,
    parameters: Vec<TypedValue>,
) -> Frame {
    let native = method.access_flags.contains(&AccessFlag::Native);

    let (local_variables_size, operand_stack_size, opcodes) = match &method.code {
        Some(code_attribute_entry) => {
            if native {
                panic!(
                    "trying to interpret native method {} on class {}, but code attribute present",
                    &method.name, class_name
                )
            }
            (
                code_attribute_entry.max_locals as usize,
                code_attribute_entry.max_stack as usize,
                code_attribute_entry.opcodes.clone(),
            )
        }
        None => {
            if !native {
                panic!(
                    "trying to interpret non-native method {} on class {}, but no code attribute present",
                    &method.name, class_name
                )
            }
            (parameters.len() + 1, 0, Vec::new())
        }
    };

    let mut frame = Frame {
        native,
        class_index,
        opcodes,
        local_variables: vec![TypedValue::Void(()); local_variables_size],
        operand_stack: Vec::with_capacity(operand_stack_size),
        return_type: method.return_type.clone(),
    };

    println!("* initialising frame");
    let mut index = 0;
    if !method.access_flags.contains(&AccessFlag::Static) {
        let instance = instance.unwrap(); //TODO: error handling?
        println!("  * putting {:?} at index {}", instance.reference, index);
        frame.local_variables[index] = TypedValue::Reference(instance.reference.clone());
        index += 1;
    }
    for p in parameters {
        let add_dup = match &p {
            TypedValue::Long(value) => Some(TypedValue::Long(*value)),
            TypedValue::Double(value) => Some(TypedValue::Double(*value)),
            _ => None,
        };

        println!("  * putting {:?} at index {}", p, index);
        frame.local_variables[index] = p;
        index += 1;
        match add_dup {
            Some(dup) => {
                frame.local_variables[index] = dup;
                index += 1;
            }
            None => (),
        }
    }

    frame
}

// my first lifetime annotation :)
fn static_field_lookup<'a>(
    runtime: &'a Runtime,
    class_name: &str,
    field_name: &str,
    field_type: &Type,
) -> &'a Field {
    let (field, _class) = static_field_lookup_internal(runtime, class_name, field_name, field_type);
    field
}

fn static_field_lookup_mut<'a>(
    runtime: &'a mut Runtime,
    class_name: &str,
    field_name: &str,
    field_type: &Type,
) -> &'a mut Field {
    let (_field, class_index) =
        static_field_lookup_internal(runtime, class_name, field_name, field_type);

    let class_mut = runtime.heap.get_class_by_index_mut(class_index);
    let field_mut = field_lookup_direct_mut(class_mut, field_name, field_type);

    field_mut.unwrap()
}

fn static_field_lookup_internal<'a>(
    runtime: &'a Runtime,
    class_name: &str,
    field_name: &str,
    field_type: &Type,
) -> (&'a Field, usize) {
    // 1) look in mentioned class
    // 2) look recursively in direct superinterfaces
    // 3) look recursively in superclasses
    // 4) fail :)

    // look in mentioned class
    let (field_class_index, field_class) = runtime.heap.get_class_by_name(class_name).unwrap();
    if let Some(f) = static_field_lookup_direct(field_class, field_name, field_type) {
        return (f, field_class_index);
    }

    // look recursively in direct superinterfaces
    if let Some((f, c)) = static_field_lookup_interfaces(
        runtime,
        &field_class.interface_names,
        field_name,
        field_type,
    ) {
        return (f, c);
    }

    // look recursively in superclasses
    if let Some((f, c)) = static_field_lookup_superclasses(
        runtime,
        &field_class.super_class_name,
        field_name,
        field_type,
    ) {
        return (f, c);
    }

    // fail :)
    panic!(
        "could not find field: {}, in class {} or superclasses and interfaces!",
        class_name, field_name
    );
}

fn static_field_lookup_interfaces<'runtime>(
    runtime: &'runtime Runtime,
    interface_names: &Vec<String>,
    field_name: &str,
    field_type: &Type,
) -> Option<(&'runtime Field, usize)> {
    // first check all direct interfaces
    for interface_name in interface_names {
        match runtime.heap.get_class_by_name(interface_name) {
            Some((interface_index, interface)) => {
                let field = static_field_lookup_direct(interface, field_name, field_type);
                if let Some(f) = field {
                    return Some((f, interface_index));
                }
            }
            None => panic!("could not find interface: {}", &interface_name),
        }
    }

    // then do recursive lookup
    for interface_name in interface_names {
        let (_interface_index, interface) =
            runtime.heap.get_class_by_name(&interface_name).unwrap();
        let field = static_field_lookup_interfaces(
            runtime,
            &interface.interface_names,
            field_name,
            field_type,
        );
        if field.is_some() {
            return field;
        }
    }

    // nothing found in interfaces
    None
}

fn static_field_lookup_superclasses<'runtime>(
    runtime: &'runtime Runtime,
    super_class_name: &str,
    field_name: &str,
    field_type: &Type,
) -> Option<(&'runtime Field, usize)> {
    // check superclass
    match runtime.heap.get_class_by_name(super_class_name) {
        Some((super_class_index, super_class)) => {
            let field = static_field_lookup_direct(super_class, field_name, field_type);
            if let Some(f) = field {
                return Some((f, super_class_index));
            }

            // we reached Object, no need to look further
            if super_class_name.eq(&super_class.super_class_name) {
                return None;
            }

            // then check superclass of superclass
            return static_field_lookup_superclasses(
                runtime,
                &super_class.super_class_name,
                field_name,
                field_type,
            );
        }
        None => panic!("could not find class: {}", &super_class_name),
    }
}

fn static_field_lookup_direct<'runtime>(
    class: &'runtime Class,
    field_name: &str,
    field_type: &Type,
) -> Option<&'runtime Field> {
    match class
        .fields
        .iter()
        .filter(|f| f.access_flags.contains(&AccessFlag::Static))
        .find(|f| f.name.eq(field_name))
    {
        Some(field) => {
            if !field.field_type.eq(field_type) {
                panic!(
                    "expected field type ({}) different than expected field type ({})",
                    &field.field_type, field_type
                );
            }
            Some(field)
        }
        None => None,
    }
}

fn field_lookup_direct_mut<'runtime>(
    class: &'runtime mut Class,
    field_name: &str,
    field_type: &Type,
) -> Option<&'runtime mut Field> {
    match class.fields.iter_mut().find(|f| f.name.eq(field_name)) {
        Some(field) => {
            if !field.field_type.eq(field_type) {
                panic!(
                    "expected field type ({}) different than expected field type ({})",
                    &field.field_type, field_type
                );
            }
            Some(field)
        }
        None => None,
    }
}

fn pop_int(operand_stack: &mut Vec<TypedValue>) -> i32 {
    let typed_value = operand_stack.pop().expect("pop on empty stack");
    match typed_value {
        TypedValue::Int(v) => v,
        TypedValue::Boolean(v) => {
            if v {
                1
            } else {
                0
            }
        }
        //TODO: probably more, because of the strange Int representation of smaller typess
        _ => panic!("expected Int value on stack, but found: {}", typed_value),
    }
}

fn pop_long(operand_stack: &mut Vec<TypedValue>) -> i64 {
    let typed_value = operand_stack.pop().expect("pop on empty stack");
    match typed_value {
        TypedValue::Long(v) => v,
        _ => panic!("expected Long value on stack, but found: {}", typed_value),
    }
}

fn pop_double(operand_stack: &mut Vec<TypedValue>) -> f64 {
    let typed_value = operand_stack.pop().expect("pop on empty stack");
    match typed_value {
        TypedValue::Double(v) => v,
        _ => panic!("expected Double value on stack, but found: {}", typed_value),
    }
}

fn pop_reference(operand_stack: &mut Vec<TypedValue>) -> ReferenceTypedValue {
    let typed_value = operand_stack.pop().expect("pop on empty stack");
    match typed_value {
        TypedValue::Reference(v) => v,
        _ => panic!(
            "expected Reference value on stack, but found: {}",
            typed_value
        ),
    }
}

fn iload(index: usize, frame: &mut Frame) {
    let value = &frame.local_variables[index];
    match value {
        TypedValue::Int(v) => frame.operand_stack.push(TypedValue::Int(*v)),
        _ => panic!("expected Int value on stack, but found: {}", value),
    }
}

fn istore(index: usize, frame: &mut Frame) {
    let value = pop_int(&mut frame.operand_stack);
    let typed_value = TypedValue::Int(value);
    println!(" {}", &typed_value);
    frame.local_variables[index] = typed_value
}

fn lload(index: usize, frame: &mut Frame) {
    let value = &frame.local_variables[index];
    match value {
        TypedValue::Long(v) => frame.operand_stack.push(TypedValue::Long(*v)),
        _ => panic!("expected Int value on stack, but found: {}", value),
    }
}

fn lstore(index: usize, frame: &mut Frame) {
    let value = pop_long(&mut frame.operand_stack);
    frame.local_variables[index] = TypedValue::Long(value)
}

fn aload(index: usize, frame: &mut Frame) {
    println!(
        "operand stack before aload {}: {:?}",
        index, frame.operand_stack
    );
    let value = &frame.local_variables[index];
    match value {
        TypedValue::Reference(v) => frame.operand_stack.push(TypedValue::Reference(v.clone())),
        _ => panic!("expected Reference value on stack, but found: {}", value),
    }
    println!(
        "operand stack after aload {}: {:?}",
        index, frame.operand_stack
    );
}

fn astore(index: usize, frame: &mut Frame) {
    let reference = pop_reference(&mut frame.operand_stack);
    println!(" {:?}", reference);
    frame.local_variables[index] = TypedValue::Reference(reference);
}
