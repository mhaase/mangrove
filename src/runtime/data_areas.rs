use std::fmt;

use crate::class_loader::class::{Class, ReferenceTypedValue, Type, TypedValue};

pub struct Frame {
    pub native: bool,
    pub class_index: usize,
    pub opcodes: Vec<u8>,
    pub local_variables: Vec<TypedValue>,
    pub operand_stack: Vec<TypedValue>,
    pub return_type: Type,
}

impl fmt::Display for Frame {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Frame {{ local variables count: {}, operand stack count: {}, opcodes: {}, native: {} }}",
            self.local_variables.len(),
            self.operand_stack.len(),
            self.opcodes.len(),
            self.native
        )
    }
}

pub struct VmThread {
    pub vm_stack: VmStack,
    pub program_counter: usize,
}

impl fmt::Display for VmThread {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "VmThread {{ program_counter: {}, vm_stack: {} }}",
            self.program_counter, self.vm_stack
        )
    }
}

pub struct VmStack {
    pub frames: Vec<Frame>,
}

impl fmt::Display for VmStack {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let frames: String = self
            .frames
            .iter()
            .map(|frame| format!("{}, ", frame))
            .collect();
        write!(f, "VmStack {{ frames: {} }}", &frames)
    }
}

pub struct Heap {
    pub method_area: Vec<Class>,
    pub instances: Vec<Instance>,
    pub array_instances: Vec<ArrayInstance>,
}

impl fmt::Display for Heap {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Heap {{ classes: {}, instances: {} }}",
            self.method_area.len(),
            self.instances.len()
        )
    }
}

impl fmt::Debug for Heap {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}\n", self)?;
        for class in &self.method_area {
            write!(f, "  - {}", class)?;
        }
        write!(f, "\n")
    }
}

impl Heap {
    pub fn get_class_by_name(&self, class_name: &str) -> Option<(usize, &Class)> {
        //TODO: should probably use a Map for that :)
        self.method_area
            .iter()
            .enumerate()
            .find(|(_index, class)| class.name.eq(&class_name))
    }

    pub fn get_class_index_by_name(&self, class_name: &str) -> Option<usize> {
        //TODO: should probably use a Map for that :)
        self.method_area
            .iter()
            .enumerate()
            .find(|(_index, class)| class.name.eq(&class_name))
            .map(|(index, _class)| index)
    }

    /// Lookup by class index. Will panic in case the index can not be found. This should
    /// never happen, since a given index should come from somewhere. We don't make them up :)
    pub fn get_class_by_index(&self, index: usize) -> &Class {
        match self.method_area.get(index) {
            Some(class) => class,
            None => panic!("could not find loaded class with index {}", index),
        }
    }

    pub fn get_class_by_index_mut(&mut self, index: usize) -> &mut Class {
        match self.method_area.get_mut(index) {
            Some(class) => class,
            None => panic!("could not find loaded class with index {}", index),
        }
    }

    pub fn get_instance_by_index(&self, index: usize) -> Option<&Instance> {
        self.instances.get(index)
    }

    pub fn get_instance_by_index_mut(&mut self, index: usize) -> Option<&mut Instance> {
        self.instances.get_mut(index)
    }

    pub fn get_array_instance_by_index(&self, index: usize) -> Option<&ArrayInstance> {
        self.array_instances.get(index)
    }

    pub fn get_array_instance_by_index_mut(&mut self, index: usize) -> Option<&mut ArrayInstance> {
        self.array_instances.get_mut(index)
    }
}

pub struct Instance {
    pub reference: ReferenceTypedValue,
    pub class_index: usize,
    pub field_values: Vec<InstanceField>,
}

pub struct InstanceField {
    pub name: String,
    pub value: TypedValue,
}

pub struct ArrayInstance {
    pub reference: ReferenceTypedValue,
    pub components: Vec<TypedValue>,
}
