package mangrove.test;

public class CurrentStarter {

    public static void main(String[] args) {
        var instance = new CurrentStarter();
        var x = instance.add(3, 20);
        var y = instance.substract(3, 20);
        var z = instance.multiply(3, 20);

        var test = "test";
    }

    int add(int a, int b) {
        var c = a + b;
        return c;
    }

    int substract(int a, int b) {
        var c = a - b;
        return c;
    }

    int multiply(int a, int b) {
        var c = a * b;
        return c;
    }
}