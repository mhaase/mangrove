package mangrove.test.invokemain;

public class ManyParameters {

    private static int s1 = 100;
    private static final char s2 = '#';

    public static void main(String[] args) {
        short s = (short) 123;
        var instance = new ManyParameters(1234, s, '?', new Object());
        var result = instance.manyParams(7999, 123456789L, 'c');

        boolean success = result == 123465022;
    }

    public ManyParameters(int a, short b, char c, Object o) {
        int x = a + b + c;
    }

    public int manyParams(int a, long b, char c) {
        int bAsInt = (int) b;
        int x = a + bAsInt + c + s1 + s2;
        return x;
    }

    public static int manyParamsStatic(int a, long b, char c) {
        return new ManyParameters(0, (short) 0, ' ', null).manyParams(7999, 123456789L, 'c');
    }
}
