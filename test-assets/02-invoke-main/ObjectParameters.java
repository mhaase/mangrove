package mangrove.test.invokemain;

public class ObjectParameters {

    public static void main(String[] args) {
    }

    public ObjectParameters(int o) {
    }

    public int objectParams(Object o) {
        return 7;
    }

    public static int test() {
        var o1 = 5;
        var instance = new ObjectParameters(o1);
        return instance.objectParams(new Object());
    }
}
