package mangrove.test.invokemain;

public class MainParameters {

    private static int length;

    public static void main(String[] args) {
        for (var arg : args) {
            length += arg.length();
        }
    }

    public static int test() {
        return length;
    }
}
