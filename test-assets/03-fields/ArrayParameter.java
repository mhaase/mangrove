package mangrove.test.fields;

public class ArrayParameter {

    public static void main(String[] args) {
    }

    public void changeArray(byte[] arg) {
        arg[0] = 0;
        arg[1] = 1;
        arg[2] = 2;
        arg[3] = 3;
    }

    public static int test() {
        var array = new byte[4];
        new ArrayParameter().changeArray(array);

        return array[0] + array[1] + array[2] + array[3];
    }
}
