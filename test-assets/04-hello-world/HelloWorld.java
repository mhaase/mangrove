package mangrove.test.helloworld;

public class HelloWorld {

    public static void main(String[] args) {
        String name = name(args);
        System.out.println("hello " + name);
    }

    private static String name(String[] args) {
        if (args == null || args.length == 0) {
            return "world";
        }
        return args[0];
    }
}