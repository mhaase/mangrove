package mangrove.test.inheritance;

public class Z extends Y {

    public Z() {
    }

    public static void main(String[] args) {
        var z = new Z();
        z.times10(12);
    }

    public int times10(int a) {
        return times5(a) * 2;
    }

    public static int times5(int a) {
        return 5 * a;
    }
}