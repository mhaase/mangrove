package mangrove.test.inheritance;

public class X {

    public X() {
        int x = -2;
    }

    public int add(int a, int b) {
        int c = a + b;
        return c;
    }
}