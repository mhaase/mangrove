package mangrove.test.inheritance;

public class Y extends X {

    public Y() {
        int x = 5;
    }

    public int times10(int a) {
        return a + a + a + a + a + a + a + a + a + a;
    }
}