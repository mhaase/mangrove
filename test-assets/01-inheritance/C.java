package mangrove.test.inheritance;

public class C extends B {

    public C() {
        int a = 9999;
        int b = 8888;
        int c = a + b;
    }

    public static void main(String[] args) {
        var c = new C();
        c.add(5);
    }

    public static int add(int a) {
        return a + a + a + a + a + a + a + a + a + a;
    }
}