package mangrove.test.inheritance;

public class B extends A {

    public B() {
        int a = 5;
        int b = 6;
        int c = a + b;
    }

    public int add(int a, int b) {
        return a + b;
    }
}