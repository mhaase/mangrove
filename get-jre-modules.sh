#!/bin/bash

EXTRACT_PATH=/tmp/eclipse-temurin-jdk
# downloading the JDK, because we need the 'jimage' binary, else the JRE would be sufficient
DOWNLOAD_PATH=https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17.0.5%2B8/OpenJDK17U-jdk_x64_linux_hotspot_17.0.5_8.tar.gz
FILE_PATH=eclipse-temurin-jdk.tar.gz

TARGET_PATH=./assets/jre-modules/

echo "Removing previously downloaded files"
rm -rf $TARGET_PATH
echo "Creating temporary folder $EXTRACT_PATH"
mkdir $EXTRACT_PATH

echo "Downloading Temurin"
curl --location $DOWNLOAD_PATH --output $EXTRACT_PATH/$FILE_PATH
echo "Extracting Temurin archive"
tar --extract --file $EXTRACT_PATH/$FILE_PATH --directory $EXTRACT_PATH/

SUB_PATH=$EXTRACT_PATH/jdk-17.0.5+8

echo "Extracting java.base module class files"
# will fail if /tmp is used and mounted with noexec flag
$SUB_PATH/bin/jimage extract --include='/java.base**' --dir=$TARGET_PATH $SUB_PATH/lib/modules

echo "Removing temporary folder"
rm -rf $EXTRACT_PATH

echo "Done."
